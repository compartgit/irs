const fs = require("fs");
const xmljs = require('xml-js');
const coldir = './includefiles/columns';

const columnController = {
    ColumnInclude: function (req, res) {
        res.sendfile('views' + '/blankcolumninclude.html');
    },
    getColumnInclude: function (req, res) {
        res.sendfile('views' + '/columninclude.html');
    },
    ColumnForm: function (req, res) {
        res.sendfile('views' + '/blankcolumn.html');
    },
    getColumnForm: function (req, res) {
        res.sendfile('views' + '/column.html');
    },
    getDownloadColFile: function (req, res) {
        var name = req.params.id;
        try {
            const data = fs.readFileSync(coldir + '/' + name, 'utf8');
            res.send(data);
          } catch (err) {
            console.error(err)
          }
    },
    getEditColumnForm: function (req, res) {
        var id = req.params.id;
        fs.readFile(coldir+'/'+ id, 'utf8', (err, data) => {
            if (err) {
            console.error(err)
            return
            }
            console.log('File Data:\n',data);
            data = data.replace(/&/g,"&amp;");
            var xmldata = data;
            var result = xmljs.xml2json(xmldata, {compact: true, spaces: 4});
            var jsondata = JSON.parse(result);
            console.log(jsondata);
            var str = '';
            str += '<html>';
            str += '<head><title>Edit Column Include File</title>';
            str += ' <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>';
            str += '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">';
            str += '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>';
            str += '<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>';
            str += '<link href="/index/api/css" rel="stylesheet">';
            str += '<script>function onSubmit() {alert("The File was updated");}</script>'
            str += '</head>';
            str += '<body>';
            str += '<div id="row">';
            str += '<form id="myform" method="post" action="'+ process.env.BASE_URL +'/index/api/posteditcolumndata" onsubmit="onSubmit()">';
            str += '<div id="form" class="container">';
            str += '<center><h1>Edit Column Include File</h1></center>';
            str += '<fieldset class="border p-2 form-group">';
            str += '<div class="form-group col-md-4 col-sm-8">';
            str += '<label>File Name:</label>';
            str += '<input class="form-control" type="text" name="filename" id="filename" value="'+ id +'"/>';
            str += '<input class="form-control" type="hidden" name="orgfilename" id="orgfilename" value="'+ id +'"/>';
            str += '</div>';
            str += '</fieldset>';
            if('column' in jsondata == true)
            {
            if(jsondata['column'].length > 1)
            {
                for(var i in jsondata['column'])
                {
                str += '<fieldset class="border p-2 form-group">';
                str += '<legend>Column</legend>';
                str += '<div class="form-group col-md-4 col-sm-8">';
                for(var j in jsondata['column'][i])
                {
        
                    for(var k in jsondata['column'][i][j])
                    {
                    str += '<label>'+ k +':</label>';
                    str += '<input class="form-control" type="text" id="column-'+ k +'" name="column-'+ k +'" value="'+ jsondata['column'][i][j][k] +'"/><br>';
                    }
                }
                str += '</div>';
                str += '</fieldset>';
                }
            }
            else
            {
                str += '<fieldset class="border p-2 form-group">';
                str += '<legend>Column</legend>';
                str += '<div class="form-group col-md-4 col-sm-8">';
                for(var i in jsondata['column']['_attributes'])
                {
                str += '<label>'+ i +':</label>';
                str += '<input class="form-control" type="text" id="column-'+ i +'" name="column-'+ i +'" value="'+ jsondata['column']['_attributes'][i] +'"/><br>';
                }
                str += '</div>';
                str += '</fieldset>';
            }
            }
            str += '</div>';
            str += '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-black" type="submit" value="Submit" />';
            str += '&nbsp;<a class="btn btn-black" href="'+ process.env.BASE_URL +'/index/api/columninclude" class="button">Cancel</a>';
            str += '</form>';
            str += '</div>';
            str += '</body>';
            str += '</html>';
            res.send(str);
        });
    },
    deleteColumnInclude: function (req, res) {
        var id = req.params.id;
        fs.unlink(coldir + '/' + id, (err) => {
        if (err) throw err;
        console.log(id + ' was deleted');
        });
        res.status(200);
    },
    postColumn: function (req, res) {
        var data = req.body;
        console.log(data);
        fs.readdir(coldir, (err, files) => {
            var filescount = files.length;
            var column = [];
            if(typeof data['column-name'] == 'object' || typeof data['column-width'] == 'object' || typeof data['column-xCoor'] == 'object') {
                for(var i = 0; i < data['column-name'].length; i++) {
                    var lst = [];
                    Object.keys(data).forEach(function(key) {
                        if(data[key][i] != '') {
                            lst.push(key);
                        }
                    });
                    var jsonobj = {};
                    for(var k = 0; k < lst.length; k++) {
                        var arr = [];
                        arr.push(lst[k].split('-'));
                        jsonobj[arr[0][1]] = data[lst[k]][i];
                    }
                    column.push({
                        "_attributes": jsonobj
                    });
                }
            } else {
                var lst = [];
                Object.keys(data).forEach(function(key) {
                    for(var j = 0; j < data[key].length; j++) {
                        if(data[key][j] != '') {
                            lst.push(key);
                        }
                    }
                });
                var jsonobj = {};
                for(var k = 0; k < lst.length; k++) {
                    var arr = [];
                    arr.push(lst[k].split('-'));
                    jsonobj[arr[0][1]] = data[lst[k]];
                }
                column.push({
                    "_attributes": jsonobj
                });
            }
            console.log(column);
            var datajson = {
                "column": column
            };
            var datafinal = JSON.stringify(datajson);
            var options = {
                compact: true,
                spaces: 4
            }
            var xml = xmljs.json2xml(datafinal, options);
            xml = xml.replace((new RegExp('&amp;', 'g')),'&');
            console.log("\nXML:\n", xml);
            fs.writeFile(coldir + '/columns.v' + (filescount + 1) + '.include', xml, function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("Created include file with Name: columns.v" + (filescount + 1) + ".include");
            });
            res.redirect('/index/api/columninclude');
        });
    },
    postEditColumn: function (req, res) {
        var data = req.body;
        console.log(data);
        var template = {};
        if(('column-name' in data) == true & data['column-name'] != '') {
            var column = [];
            var columnjson = {};
            if(typeof data['column-name'] == 'object') 
            {
                for(var i = 0; i < data['column-name'].length; i++) {
                    var columnobj = {};
                    columnobj['name'] = data['column-name'][i];
                    if(('column-xCoor' in data) == true & data['column-xCoor'] != '') {
                        if(typeof data['column-xCoor'] == 'object') {
                            columnobj['xCoor'] = data['column-xCoor'][i];
                        } else {
                            columnobj['xCoor'] = data['column-xCoor'];
                        }
                    }
                    if(('column-width' in data) == true & data['column-width'] != '') {
                        if(typeof data['column-width'] == 'object') {
                            columnobj['width'] = data['column-width'][i];
                        } else {
                            console.log(data['column-width']);
                            columnobj['width'] = data['column-width'];
                        }
                    }
                    columnjson = {
                        "_attributes": columnobj
                    }
                    column.push(columnjson);
                }
                template['column'] = column;
            }
            else 
            {
                var columnobj = {};
                columnobj['name'] = data['column-name'];
                if(('column-xCoor' in data) == true & data['column-xCoor'] != '') {
                    columnobj['xCoor'] = data['column-xCoor'];
                }
                if(('column-width' in data) == true & data['column-width'] != '') {
                    columnobj['width'] = data['column-width'];
                }
                template['column'] = {
                    "_attributes": columnobj
                }
            }
            var datajson = {"column": template['column']};
            console.log(datajson);
            var datafinal = JSON.stringify(datajson);
            var options = {
                compact: true,
                spaces: 4
            }
            var xml = xmljs.json2xml(datafinal, options);
            xml = xml.replace((new RegExp('&amp;', 'g')),'&');
            console.log("\nXML:\n", xml);
            fs.unlink(coldir + '/' + data['orgfilename'], (err) => {
                if (err) throw err;
            });
            fs.writeFile(coldir + '/'+ data['filename'], xml, function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("Updated file with Name: "+ data['filename']);
            });
            res.redirect('/index/api/columninclude');
        }
    },
    postImportColumn: function (req, res) {
        res.redirect('/api/columninclude');
    }
};

module.exports = columnController;