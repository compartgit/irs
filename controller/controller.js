const Model = require('../model/model');
const fs = require("fs");
const dotenv = require('dotenv');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const coldir = './includefiles/columns';
const segdir = './includefiles/segments';
const pgdir = './includefiles/pagetype';
const tempdir = './templatefiles';

// Set up Global configuration access
dotenv.config();

const Controller = {
    getRegister: function (req, res) {
        res.sendfile('views' + '/register.html');
    },
    getLogin: function (req, res) {
        res.sendfile('views' + '/login.html');
    },
    getForgotPassword: function (req, res) {
        res.sendfile('views' + '/forgotpassword.html');
    },
    getUserInfo: function (req, res) {
        var email = req.params.id;
        Model.UserInfo(email, function (err, data) {
            if(err) throw err;
            if(data.length>= 1)
            {
                var userdata = data[0];
                userdata['otp'] = generateOTP();

                sendEmailOTP(userdata['otp'], userdata['email']);
                    
                res.status(200).send(userdata);
            }
            else
            {
                res.status(404).send('showError');
            }
        });
    },
    getScript: function (req, res) {
        res.sendfile('public/javascripts/' + '/script.js');
    },
    getStylesheet: function (req, res) {
        res.sendfile('public/stylesheets/' + '/style.css');
    },
    getFileNames: function (req, res) {

        var colFileNames = fs.readdirSync(coldir);
        var segFileNames = fs.readdirSync(segdir);
        var pgFileNames = fs.readdirSync(pgdir);
        var cpFileNames = fs.readdirSync(tempdir);
        var files = {
            "pagetype": pgFileNames,
            "segment": segFileNames,
            "column": colFileNames,
            "cpFiles": cpFileNames
        }
        res.send(files);
    },
    postRegister: function (req, res) {
        var data = req.body;
        var inputData = {
            name: data.name,
            email: data.email,
            password: data.password
        }

        const password = inputData['password'];

        // Encryption of the string password
        bcrypt.genSalt(10, function (err, Salt) {

            // The bcrypt is used for encrypting password.
            bcrypt.hash(password, Salt, function (err, hash) {

                if (err) {
                    return console.log('Cannot encrypt');
                }

                inputData['password'] = hash;

                Model.UserInfo(inputData.email, function (err, data) {
                    if(err) throw err
                    if(data.length>=1)
                    {
                        res.status(400).send('showError');
                    }
                    else
                    {
                        Model.InsertUser(inputData, function (err) {
                            if (err) throw err;
                        });
                        res.status(200).send('showOK');
                    }
                });

            });
        });
    },
    postForgotPassword: function (req, res) {
        var data = req.body;
        var email = data.email;
        var password = data.password;

        // Encryption of the string password
        bcrypt.genSalt(10, function (err, Salt) {

            // The bcrypt is used for encrypting password.
            bcrypt.hash(password, Salt, function (err, hash) {

                if (err) {
                    return console.log('Cannot encrypt');
                }

                var hashpassword = hash;

                Model.UpdatePassword(hashpassword, email, function (err, data) {
                    if (err) throw err;
                    if (data.affectedRows == 1)
                    {
                        res.status(200).send('showOK');
                    }
                });
            });
        });
    },
    postLogin: function (req, res) {
        var data = req.body;
        var email = data.email;
        var password = data.password;

        Model.UserInfo(email, function (err, data) {
            if(err) throw err;

            if(data.length == 1)
            {

                const hashedPassword = data[0].password;

                bcrypt.compare(password, hashedPassword,
                    async function (err, isMatch) {
        
                    // Comparing the original password to
                    // encrypted password
                    if (isMatch) 
                    {
                        var name = data[0].name;
                        let jwtSecretKey = process.env.JWT_SECRET_KEY;
                        let tokendata = {
                            email: email,
                            password: hashedPassword,
                            name: name
                        }
            
                        const token = jwt.sign(tokendata, jwtSecretKey, {expiresIn: "10h"});

                        var resdata = {
                            "token": token,
                            "name": name,
                            "email": email
                        }
            
                        res.status(200).send(resdata);
                    }
        
                    if (!isMatch) 
                    {
                        // Error
                        res.status(400).send('showError');
                    }
                });
            }
            else{
                // Access Denied
                return res.status(400).send('showError');
            }
        });
    },
    Profile: function (req, res) {
        res.sendfile('views' + '/blankprofile.html');
    },
    getProfile: function (req, res) {
        res.sendfile('views' + '/profile.html');
    },
    postProfile: function (req, res) {
        var data = req.body;
        Model.UserInfo(data.email, function (err, result) {
            if(err) throw err;
            if(data['password'] == '')
                {
                    Model.UpdateUser(data.name, data.email, data.originalemail, function (err) {
                        if (err) throw err;
                        else res.status(200).send('showOK');
                    });
                }
                else
                {
                    const password = data['password'];
        
                    // Encryption of the string password
                    bcrypt.genSalt(10, function (err, Salt) {
        
                    // The bcrypt is used for encrypting password.
                    bcrypt.hash(password, Salt, function (err, hash) {
        
                        if (err) {
                            return console.log('Cannot encrypt');
                        }
        
                        data['password'] = hash;
                        Model.UpdateUser1(data.name, data.email, data.password, data.originalemail, function (err) {
                            if (err) throw err;
                            else res.status(200).send('showLogin');
                        });
        
                    });
                });
            }
        });
    }
};

function generateOTP() {
    var otp = Math.floor(1000 + Math.random() * 9000);
    console.log(otp);
    return otp;
}

function sendEmailOTP(otp, email) {
    var transporter = nodemailer.createTransport({
        service: process.env.MAIL_SERVICE,
        auth: {
        user: process.env.AUTH_USER,
        pass: process.env.AUTH_PASS
        }
    });
    
    var mailOptions = {
        from: process.env.AUTH_USER,
        to: email,
        subject: process.env.MAIL_SUBJECT,
        html: '<h3>Hello,<br>Please use the verification code below for password reset:</h3><h2>' + otp + '</h2><h3>If you didn\'t request this, you can ignore this email or let us know.<br>Thanks! <br>IRS team</h3>'
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
        console.log(error);
        res.send(error);
        } else {
        console.log('Email sent: ' + info.response);
        }
    });
}

module.exports = Controller;