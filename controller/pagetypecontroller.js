const fs = require("fs");
const xmljs = require('xml-js');
const pgdir = './includefiles/pagetype';

const pagetypeController = {
    PageTypeInclude: function (req, res) {
        res.sendfile('views' + '/blankpagetypeinclude.html');
    },
    getPageTypeInclude: function (req, res) {
        res.sendfile('views' + '/pagetypeinclude.html');
    },
    PageTypeForm: function (req, res) {
        res.sendfile('views' + '/blankpagetype.html');
    },
    getPageTypeForm: function (req, res) {
        res.sendfile('views' + '/pagetype.html');
    },
    getDownloadPgFile: function (req, res) {
        var name = req.params.id;
        try {
            const data = fs.readFileSync(pgdir + '/' + name, 'utf8');
            res.send(data);
          } catch (err) {
            console.error(err)
          }
    },
    getEditPageTypeForm: function (req, res) {
        var id = req.params.id;
        fs.readFile(pgdir+'/'+ id, 'utf8', (err, data) => {
            if (err) {
            console.error(err)
            return
            }
            console.log('File Data:\n',data);
            data = data.replace(/&/g,"&amp;");
            var xmldata = data;
            var result = xmljs.xml2json(xmldata, {compact: true, spaces: 4});
            var jsondata = JSON.parse(result);
            console.log(jsondata);
            var str = '';
            str += '<html>';
            str += '<head><title>Edit PageType Include File</title>';
            str += ' <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>';
            str += '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">';
            str += '<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>';
            str += '<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>';
            str += '<link href="/index/api/css" rel="stylesheet">';
            str += '<script>function onSubmit() {alert("The File was updated");}</script>'
            str += '</head>';
            str += '<body>';
            str += '<div id="row">';
            str += '<form id="myform" method="post" action="'+ process.env.BASE_URL +'/index/api/posteditpagetypedata" onsubmit="onSubmit()">';
            str += '<div id="form" class="container">';
            str += '<center><h1>Edit PageType Include File</h1></center>';
            str += '<fieldset class="border p-2 form-group">';
            str += '<div class="form-group col-md-4 col-sm-8">';
            str += '<label>File Name:</label>';
            str += '<input class="form-control" type="text" name="filename" id="filename" value="'+ id +'"/>';
            str += '<input class="form-control" type="hidden" name="orgfilename" id="orgfilename" value="'+ id +'"/>';
            str += '</div>';
            str += '</fieldset>';
            if(jsondata['pageType'].length > 1)
            {
            for(var pg=0;pg<jsondata['pageType'].length;pg++)
            {
                str += '<fieldset class="border p-2 form-group">';
                str += '<legend>PageType</legend>';
                str += '<div class="form-group col-md-4 col-sm-8">';
            for(var i in jsondata['pageType'][pg]['_attributes'])
                {
                    str += '<label>'+ i +':</label>';
                    str += '<input class="form-control" type="text" id="pageType-'+ i + (pg + 1) +'" name="pageType-'+ i + (pg + 1) +'" value="'+ jsondata['pageType'][pg]['_attributes'][i] +'"/><br>';
                }
                str += '</div>';
                if(jsondata['pageType'][pg]['segment'].length > 1)
                {
                    for(var i=0;i<jsondata['pageType'][pg]['segment'].length;i++)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>Segment</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var j in jsondata['pageType'][pg]['segment'][i]['_attributes'])
                    {
                        str += '<label>'+ j +':</label>';
                        str += '<input class="form-control" type="text" id="segment-'+ j + (pg + 1) + (i + 1) +'" name="segment-'+ j + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['_attributes'][j] +'"/><br>';
                    }
                    str += '</div>';
                    if('segmentProperties' in jsondata['pageType'][pg]['segment'][i] == true)
                    {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Segment Properties</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['_attributes'])
                        {
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" name="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['_attributes'][k] +'"/><br>';
                        }
                        str += '</div>';
                        if('coordinates' in jsondata['pageType'][pg]['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Coordinates</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['coordinates']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="coordinates-'+ k + (pg + 1) + (i + 1)+'" name="coordinates-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['coordinates']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('header' in jsondata['pageType'][pg]['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Header</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['header']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="header-'+ k + (pg + 1) + (i + 1)+'" name="header-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['header']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('image' in jsondata['pageType'][pg]['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Image</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['image']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1)+'" name="image-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['image']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('listMarks' in jsondata['pageType'][pg]['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>ListMarks</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['listMarks']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1)+'" name="listMarks-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
                        if('subSegment' in jsondata['pageType'][pg]['segment'][i]['segmentProperties'] == true)
                        {
                        if(jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'].length > 1)
                        {
                            for(var subseg=0;subseg<jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'].length;subseg++)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>SubSegment</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['_attributes'])
                            {
                                
                                str += '<label>'+ j +':</label>';
                                str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subSegmentProperties</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'][k] +'"/><br>';
                                }
                                str += '</div>';
                                if('subCoordinates' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('image' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('listMarks' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                                str += '</fieldset>';
                            }
                            }
                            str += '</fieldset>';
                        }
                        else
                        {
                            var subseg = 0;
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>SubSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['_attributes'])
                            {
                            
                            str += '<label>'+ j +':</label>';
                            str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'] == true)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegmentProperties</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'])
                            {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'][k] +'"/><br>';
                            }
                            str += '</div>';
                            if('subCoordinates' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('image' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('listMarks' in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '</fieldset>';
                        }
                        }
                        str += '</fieldset>';
                    }
                    str += '</fieldset>';              
                    }
                    str += '<input type="hidden" name="pgsegcount" id="pgsegcount" value="'+ jsondata['pageType'][pg]['segment'].length +'"/>';
                }
                else
                {
                    var i = 0;
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>Segment</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var j in jsondata['pageType'][pg]['segment']['_attributes'])
                    {
                    str += '<label>'+ j +':</label>';
                    str += '<input class="form-control" type="text" id="segment-'+ j + (pg + 1) + (i + 1) +'" name="segment-'+ j + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['_attributes'][j] +'"/><br>';
                    }
                    str += '</div>';
                if('segmentProperties' in jsondata['pageType'][pg]['segment'] == true)
                {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>segmentProperties</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['_attributes'])
                    {
                    str += '<label>'+ k +':</label>';
                    str += '<input class="form-control" type="text" id="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" name="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['_attributes'][k] +'"/><br>';
                    }
                    str += '</div>';
                    if('coordinates' in jsondata['pageType'][pg]['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>coordinates</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['coordinates']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="coordinates-'+ k + (pg + 1) + (i + 1)+'" name="coordinates-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['coordinates']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('header' in jsondata['pageType'][pg]['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>header</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['header']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="header-'+ k + (pg + 1) + (i + 1)+'" name="header-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['header']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('image' in jsondata['pageType'][pg]['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>image</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['image']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1)+'" name="image-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['image']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('listMarks' in jsondata['pageType'][pg]['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>listMarks</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['listMarks']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1)+'" name="listMarks-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
                    
                    if('subSegment' in jsondata['pageType'][pg]['segment']['segmentProperties'] == true)
                        {
                        if(jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'].length > 1)
                        {
                            for(var subseg=0;subseg<jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'].length;subseg++)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['_attributes'])
                            {
                                
                                str += '<label>'+ j +':</label>';
                                str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subSegmentProperties</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'][k] +'"/><br>';
                                }
                                str += '</div>';
                                if('subCoordinates' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('image' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div';
                                str += '</fieldset>';
                                }
                
                                if('listMarks' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="'+ jsondata['pageType'][pg]['segment'][i]['segmentProperties']['subSegment'].length +'"/>';
                        }
                        else
                        {
                            var subseg = 0;
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['_attributes'])
                            {
                            
                            str += '<label>'+ j +':</label>';
                            str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment'] == true)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegmentProperties</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'])
                            {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'][k] +'"/><br>';
                            }
                            str += '</div>';
                            if('subCoordinates' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('image' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('listMarks' in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType'][pg]['segment']['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="1"/>';
                        }
                        }
                        str += '</fieldset>';
                }
                str += '</fieldset>';
                str += '<input type="hidden" name="pgsegcount" id="pgsegcount" value="1"/>';
                }
                }
                str += '<input type="hidden" name="pgcount" id="pgcount" value="'+ jsondata['pageType'].length +'"/>';
            }
            else
            {
            var pg = 0;
            str += '<fieldset class="border p-2 form-group">';
            str += '<legend>PageType</legend>';
            str += '<div class="form-group col-md-4 col-sm-8">';
            for(var i in jsondata['pageType']['_attributes'])
                {
                    str += '<label>'+ i +':</label>';
                    str += '<input class="form-control" type="text" id="pageType-'+ i + (pg + 1) +'" name="pageType-'+ i + (pg + 1) +'" value="'+ jsondata['pageType']['_attributes'][i] +'"/><br>';
                }
                str += '</div>';
                if(jsondata['pageType']['segment'].length > 1)
                {
                    for(var i=0;i<jsondata['pageType']['segment'].length;i++)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>Segment</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var j in jsondata['pageType']['segment'][i]['_attributes'])
                    {
                        
                        str += '<label>'+ j +':</label>';
                        str += '<input class="form-control" type="text" id="segment-'+ j + (pg + 1) + (i + 1) +'" name="segment-'+ j + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType']['segment'][i]['_attributes'][j] +'"/><br>';
                    }
                    str += '</div>';
                    if('segmentProperties' in jsondata['pageType']['segment'][i] == true)
                    {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Segment Properties</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['_attributes'])
                        {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" name="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['_attributes'][k] +'"/><br>';
                        }
                        str += '</div>';
                        if('coordinates' in jsondata['pageType']['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Coordinates</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['coordinates']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="coordinates-'+ k + (pg + 1) + (i + 1)+'" name="coordinates-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['coordinates']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('header' in jsondata['pageType']['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Header</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['header']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="header-'+ k + (pg + 1) + (i + 1)+'" name="header-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['header']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('image' in jsondata['pageType']['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>Image</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['image']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1)+'" name="image-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['image']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
            
                        if('listMarks' in jsondata['pageType']['segment'][i]['segmentProperties'] == true)
                        {
                        str += '<fieldset class="border p-2 form-group">';
                        str += '<legend>ListMarks</legend>';
                        str += '<div class="form-group col-md-4 col-sm-8">';
                        for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['listMarks']['_attributes'])
                        {
                            
                            str += '<label>'+ k +':</label>';
                            str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1)+'" name="listMarks-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                            
                        }
                        str += '</div>';
                        str += '</fieldset>';
                        }
                        if('subSegment' in jsondata['pageType']['segment'][i]['segmentProperties'] == true)
                        {
                        if(jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'].length > 1)
                        {
                            for(var subseg=0;subseg<jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'].length;subseg++)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subSegment</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['_attributes'])
                            {
                                
                                str += '<label>'+ j +':</label>';
                                str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subSegmentProperties</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'][k] +'"/><br>';
                                }
                                str += '</div>';
                                if('subCoordinates' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('image' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('listMarks' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                                str += '</fieldset>';
                            }
                            }
                            str += '</fieldset>';
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'].length +'"/>';
                        }
                        else
                        {
                            var subseg = 0;
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>SubSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['_attributes'])
                            {
                            
                            str += '<label>'+ j +':</label>';
                            str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment'] == true)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegmentProperties</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'])
                            {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'][k] +'"/><br>';
                            }
                            str += '</div>';
                            if('subCoordinates' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('image' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('listMarks' in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment'][i]['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="1"/>';
                        }
                        
                        }
                        str += '</fieldset>'; 
                    }
                    str += '</fieldset>';              
                    }
                    str += '<input type="hidden" name="pgsegcount" id="pgsegcount" value="'+ jsondata['pageType']['segment'].length +'"/>';
                }
                else
                {
                    var i = 0;
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>Segment</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var j in jsondata['pageType']['segment']['_attributes'])
                    {
                    
                    str += '<label>'+ j +':</label>';
                    str += '<input class="form-control" type="text" id="segment-'+ j + (pg + 1) + (i + 1) +'" name="segment-'+ j + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType']['segment']['_attributes'][j] +'"/><br>';
                    }
                    str += '</div>';
                if('segmentProperties' in jsondata['pageType']['segment'] == true)
                {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>segmentProperties</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType']['segment']['segmentProperties']['_attributes'])
                    {
                    
                    str += '<label>'+ k +':</label>';
                    str += '<input class="form-control" type="text" id="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" name="segmentProperties-'+ k + (pg + 1) + (i + 1) +'" value="'+ jsondata['pageType']['segment']['segmentProperties']['_attributes'][k] +'"/><br>';
                    }
                    str += '</div>';
        
                    if('coordinates' in jsondata['pageType']['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>coordinates</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType']['segment']['segmentProperties']['coordinates']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="coordinates-'+ k + (pg + 1) + (i + 1)+'" name="coordinates-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['coordinates']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('header' in jsondata['pageType']['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>header</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType']['segment']['segmentProperties']['header']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="header-'+ k + (pg + 1) + (i + 1)+'" name="header-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['header']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('image' in jsondata['pageType']['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>image</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType']['segment']['segmentProperties']['image']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1)+'" name="image-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['image']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
        
                    if('listMarks' in jsondata['pageType']['segment']['segmentProperties'] == true)
                    {
                    str += '<fieldset class="border p-2 form-group">';
                    str += '<legend>listMarks</legend>';
                    str += '<div class="form-group col-md-4 col-sm-8">';
                    for(var k in jsondata['pageType']['segment']['segmentProperties']['listMarks']['_attributes'])
                    {
                        
                        str += '<label>'+ k +':</label>';
                        str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1)+'" name="listMarks-'+ k + (pg + 1) + (i + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                        
                    }
                    str += '</div>';
                    str += '</fieldset>';
                    }
                    
                    if('subSegment' in jsondata['pageType']['segment']['segmentProperties'] == true)
                        {
                        if(jsondata['pageType']['segment']['segmentProperties']['subSegment'].length > 1)
                        {
                            for(var subseg=0;subseg<jsondata['pageType']['segment']['segmentProperties']['subSegment'].length;subseg++)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['_attributes'])
                            {
                                
                                str += '<label>'+ j +':</label>';
                                str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subSegmentProperties</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['_attributes'][k] +'"/><br>';
                                }
                                str += '</div>';
                                if('subCoordinates' in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('image' in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                
                                if('listMarks' in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties'] == true)
                                {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                    
                                    str += '<label>'+ k +':</label>';
                                    str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'][subseg]['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                    
                                }
                                str += '</div>';
                                str += '</fieldset>';
                                }
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment'].length +'"/>';
                        }
                        else
                        {
                            var subseg = 0;
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegment</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var j in jsondata['pageType']['segment']['segmentProperties']['subSegment']['_attributes'])
                            {
                            
                            str += '<label>'+ j +':</label>';
                            str += '<input class="form-control" type="text" id="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegment-'+ j + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment']['_attributes'][j] +'"/><br>';
                            }
                            str += '</div>';
                            if('subSegmentProperties' in jsondata['pageType']['segment']['segmentProperties']['subSegment'] == true)
                            {
                            str += '<fieldset class="border p-2 form-group">';
                            str += '<legend>subSegmentProperties</legend>';
                            str += '<div class="form-group col-md-4 col-sm-8">';
                            for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'])
                            {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" name="subSegmentProperties-'+ k + (pg + 1) + (i + 1) + (subseg + 1) +'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['_attributes'][k] +'"/><br>';
                            }
                            str += '</div>';
                            if('subCoordinates' in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>subCoordinates</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subCoordinates-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('image' in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>image</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="image-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="subimage-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['image']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
            
                            if('listMarks' in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties'] == true)
                            {
                                str += '<fieldset class="border p-2 form-group">';
                                str += '<legend>listMarks</legend>';
                                str += '<div class="form-group col-md-4 col-sm-8">';
                                for(var k in jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'])
                                {
                                
                                str += '<label>'+ k +':</label>';
                                str += '<input class="form-control" type="text" id="listMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" name="sublistMarks-'+ k + (pg + 1) + (i + 1) + (subseg + 1)+'" value="'+ jsondata['pageType']['segment']['segmentProperties']['subSegment']['subSegmentProperties']['listMarks']['_attributes'][k] +'"/><br>';
                                
                                }
                                str += '</div>';
                                str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            }
                            str += '</fieldset>';
                            str += '<input type="hidden" name="pgsubsegcount" id="pgsubsegcount" value="1"/>';
                        }
                        }
                    str += '</fieldset>';
                }
                str += '</fieldset>';
                str += '<input type="hidden" name="pgsegcount" id="pgsegcount" value="1"/>';
                }
                str += '</fieldset>';
                str += '<input type="hidden" name="pgcount" id="pgcount" value="1"/>';
            }
            str += '</div>';
            str += '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-black" type="submit" value="Submit" />';
            str += '&nbsp;<a class="btn btn-black" href="'+ process.env.BASE_URL +'/index/api/pagetypeinclude" class="button">Cancel</a>';
            str += '</form>';
            str += '</div>';
            str += '</body>';
            str += '</html>';
            res.send(str);
        });
    },
    deletePageTypeInclude: function (req, res) {
        var id = req.params.id;
        fs.unlink(pgdir + '/' + id, (err) => {
        if (err) throw err;
        console.log(id + ' was deleted');
        });
        res.status(200);
    },
    postPageType: function (req, res) {
        var data = req.body;
        console.log(data);
        var pagetypecount = parseInt(data.pagetypecount);
        var segcount = parseInt(data.pgsegcount);
        var subsegcount = parseInt(data.pgsubsegcount);
        fs.readdir(pgdir, (err, files) => {
            
            var pagetype = [];
            var segment = [];
            var subseg = [];
            var pagetypejson = {};
            var segmentjson = {};
            for(var page = 1; page <= pagetypecount; page++) {
                console.log(page);
                console.log(data['pageType-name' + page]);
                var pagetypeobj = {};
                pagetypeobj['name'] = data['pageType-name' + page];
                pagetypejson = {
                    "_attributes": pagetypeobj
                };
                for(var cnt = 1; cnt <= segcount; cnt++) {
                    if(('segment-name' + page + cnt in data) == true & data['segment-name' + page + cnt] != '') {
                    console.log(data['segment-name' + page + cnt]);
                    var segmentobj = {};
                    segmentobj['name'] = data['segment-name' + page + cnt];
                    segmentjson = {
                        "_attributes": segmentobj
                    };
                    if(('segmentProperties-type' + page + cnt in data) == true & data['segmentProperties-type' + page + cnt] != '') {
                        console.log(data['segmentProperties-type' + page + cnt]);
                        var segmentPropertiesobj = {};
                        segmentPropertiesobj['type'] = data['segmentProperties-type' + page + cnt];
                        data['segmentProperties-type' + page + cnt] = '';
                        if(('segmentProperties-duplicate' + page + cnt in data) == true & data['segmentProperties-duplicate' + page + cnt] != '') {
                            console.log(data['segmentProperties-duplicate' + page + cnt]);
                            segmentPropertiesobj['duplicate'] = data['segmentProperties-duplicate' + page + cnt];
                            data['segmentProperties-duplicate' + page + cnt] = '';
                        }
                        if(('segmentProperties-oneLineRowTable' + page + cnt in data) == true & data['segmentProperties-oneLineRowTable' + page + cnt] != '') {
                            console.log(data['segmentProperties-oneLineRowTable' + page + cnt]);
                            segmentPropertiesobj['oneLineRowTable'] = data['segmentProperties-oneLineRowTable' + page + cnt];
                            data['segmentProperties-oneLineRowTable' + page + cnt] = '';
                        }
                        if(('segmentProperties-complexTable' + page + cnt in data) == true & data['segmentProperties-complexTable' + page + cnt] != '') {
                            console.log(data['segmentProperties-complexTable' + page + cnt]);
                            segmentPropertiesobj['complexTable'] = data['segmentProperties-complexTable' + page + cnt];
                            data['segmentProperties-complexTable' + page + cnt] = '';
                        }
                        if(('segmentProperties-NeedMergeText' + page + cnt in data) == true & data['segmentProperties-NeedMergeText' + page + cnt] != '') {
                            console.log(data['segmentProperties-NeedMergeText' + page + cnt]);
                            segmentPropertiesobj['NeedMergeText'] = data['segmentProperties-NeedMergeText' + page + cnt];
                            data['segmentProperties-NeedMergeText' + page + cnt] = '';
                        }
                        if(('segmentProperties-phaseADesignTable' + page + cnt in data) == true & data['segmentProperties-phaseADesignTable' + page + cnt] != '') {
                            console.log(data['segmentProperties-phaseADesignTable'] + page + cnt);
                            segmentPropertiesobj['phaseADesignTable'] = data['segmentProperties-phaseADesignTable' + page + cnt];
                            data['segmentProperties-phaseADesignTable' + page + cnt] = '';
                        }
                        if(('segmentProperties-processed' + page + cnt in data) == true & data['segmentProperties-processed' + page + cnt] != '') {
                            console.log(data['segmentProperties-processed' + page + cnt]);
                            segmentPropertiesobj['processed'] = data['segmentProperties-processed' + page + cnt];
                            data['segmentProperties-processed' + page + cnt] = '';
                        }
                        segmentjson['segmentProperties'] = {
                            "_attributes": segmentPropertiesobj
                        };
                        if(('coordinates-column' + page + cnt in data) == true & data['coordinates-column' + page + cnt] != '') {
                            console.log(data['coordinates-column' + page + cnt]);
                            var coordinatesobj = {};
                            coordinatesobj['column'] = data['coordinates-column' + page + cnt];
                            data['coordinates-column' + page + cnt] = '';
                            if(('coordinates-calculate' + page + cnt in data) == true & data['coordinates-calculate' + page + cnt] != '') {
                                console.log(data['coordinates-calculate' + page + cnt]);
                                coordinatesobj['calculate'] = data['coordinates-calculate' + page + cnt];
                                data['coordinates-calculate' + page + cnt] = '';
                            }
                            if(('coordinates-closePhrase' + page + cnt in data) == true & data['coordinates-closePhrase' + page + cnt] != '') {
                                console.log(data['coordinates-closePhrase' + page + cnt]);
                                coordinatesobj['closePhrase'] = data['coordinates-closePhrase' + page + cnt];
                                data['coordinates-closePhrase' + page + cnt] = '';
                            }
                            if(('coordinates-hLen' + page + cnt in data) == true & data['coordinates-hLen' + page + cnt] != '') {
                                console.log(data['coordinates-hLen' + page + cnt]);
                                coordinatesobj['hLen'] = data['coordinates-hLen' + page + cnt];
                                data['coordinates-hLen' + page + cnt] = '';
                            }
                            if(('coordinates-setCoordinates' + page + cnt in data) == true & data['coordinates-setCoordinates' + page + cnt] != '') {
                                console.log(data['coordinates-setCoordinates' + page + cnt]);
                                coordinatesobj['setCoordinates'] = data['coordinates-setCoordinates' + page + cnt];
                                data['coordinates-setCoordinates' + page + cnt] = '';
                            }
                            if(('coordinates-yCoor' + page + cnt in data) == true & data['coordinates-yCoor' + page + cnt] != '') {
                                console.log(data['coordinates-yCoor' + page + cnt]);
                                coordinatesobj['yCoor'] = data['coordinates-yCoor' + page + cnt];
                                data['coordinates-yCoor' + page + cnt] = '';
                            }
                            segmentjson['segmentProperties']['coordinates'] = {
                                "_attributes": coordinatesobj
                            };
                        }
                        if(('header-active' + page + cnt in data) == true & data['header-active' + page + cnt] != '') {
                            console.log(data['header-active' + page + cnt]);
                            var headerobj = {};
                            headerobj['active'] = data['header-active' + page + cnt];
                            data['header-active' + page + cnt] = '';
                            if(('header-level' + page + cnt in data) == true & data['header-level' + page + cnt] != '') {
                                console.log(data['header-level' + page + cnt]);
                                headerobj['level'] = data['header-level' + page + cnt];
                                data['header-level' + page + cnt] = '';
                            }
                            if(('header-bookmarkText' + page + cnt in data) == true & data['header-bookmarkText' + page + cnt] != '') {
                                console.log(data['header-bookmarkText' + page + cnt]);
                                headerobj['bookmarkText'] = data['header-bookmarkText' + page + cnt];
                                data['header-bookmarkText' + page + cnt] = '';
                            }
                            if(('header-type' + page + cnt in data) == true & data['header-type' + page + cnt] != '') {
                                console.log(data['header-type' + page + cnt]);
                                headerobj['type'] = data['header-type' + page + cnt];
                                data['header-type' + page + cnt] = '';
                            }
                            segmentjson['segmentProperties']['header'] = {
                                "_attributes": headerobj
                            };
                        }
                        if(('image-altText' + page + cnt in data) == true & data['image-altText' + page + cnt] != '') {
                            console.log(data['image-altText' + page + cnt]);
                            var imageobj = {};
                            imageobj['altText'] = data['image-altText' + page + cnt];
                            data['image-altText' + page + cnt] = '';
                            segmentjson['segmentProperties']['image'] = {
                                "_attributes": imageobj
                            };
                        }
                        if(('listMarks-altText' + page + cnt in data) == true & data['listMarks-altText' + page + cnt] != '') {
                            console.log(data['listMarks-altText' + page + cnt]);
                            var listMarksobj = {};
                            listMarksobj['altText'] = data['listMarks-altText' + page + cnt];
                            data['listMarks-altText' + page + cnt] = '';
                            if(('listMarks-type' + page + cnt in data) == true & data['listMarks-type' + page + cnt] != '') {
                                console.log(data['listMarks-type' + page + cnt]);
                                listMarksobj['type'] = data['listMarks-type' + page + cnt];
                                data['listMarks-type' + page + cnt] = '';
                            }
                            segmentjson['segmentProperties']['listMarks'] = {
                                "_attributes": listMarksobj
                            };
                        }
                        for(var x = 1; x <= subsegcount; x++) {
                            if(('subSegment-name' + (page) + (cnt) + (x) in data) == true & data['subSegment-name' + (page) + (cnt) + (x)] != '') {
                                console.log(data['subSegment-name' + (page) + (cnt) + (x)]);
                                var subSegmentobj = {};
                                subSegmentobj['name'] = data['subSegment-name' + (page) + (cnt) + (x)];
                                data['subSegment-name' + (page) + (cnt) + (x)] = '';
                                segmentjson['segmentProperties']['subSegment'] = {
                                    "_attributes": subSegmentobj
                                };
                                if(('subSegmentProperties-type' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-type' + (page) + (cnt) + (x)] != '') {
                                    console.log(data['subSegmentProperties-type' + (page) + (cnt) + (x)]);
                                    var subSegmentPropertiesobj = {};
                                    subSegmentPropertiesobj['type'] = data['subSegmentProperties-type' + (page) + (cnt) + (x)];
                                    data['subSegmentProperties-type' + (page) + (cnt) + (x)] = '';
                                    if(('subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)]);
                                        subSegmentPropertiesobj['oneLineRowTable'] = data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)];
                                        data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)] = '';
                                    }
                                    if(('subSegmentProperties-complexTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)]);
                                        subSegmentPropertiesobj['complexTable'] = data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)];
                                        data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)] = '';
                                    }
                                    if(('subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)]);
                                        subSegmentPropertiesobj['NeedMergeText'] = data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)];
                                        data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)] = '';
                                    }
                                    if(('subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)]);
                                        subSegmentPropertiesobj['phaseADesignTable'] = data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)];
                                        data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)] = '';
                                    }
                                    segmentjson['segmentProperties']['subSegment']['subSegmentProperties'] = {
                                        "_attributes": subSegmentPropertiesobj
                                    };
                                    if(('subCoordinates-calculate' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-calculate' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subCoordinates-calculate' + (page) + (cnt) + (x)]);
                                        var subCoordinatesobj = {};
                                        subCoordinatesobj['calculate'] = data['subCoordinates-calculate' + (page) + (cnt) + (x)];
                                        data['subCoordinates-calculate' + (page) + (cnt) + (x)] = '';
                                        if(('subCoordinates-column' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-column' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subCoordinates-column' + (page) + (cnt) + (x)]);
                                            subCoordinatesobj['column'] = data['subCoordinates-column' + (page) + (cnt) + (x)];
                                            data['subCoordinates-column' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subCoordinates-closePhrase' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-closePhrase' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subCoordinates-closePhrase' + (page) + (cnt) + (x)]);
                                            subCoordinatesobj['closePhrase'] = data['subCoordinates-closePhrase' + (page) + (cnt) + (x)];
                                            data['subCoordinates-closePhrase' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subCoordinates-hLen' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-hLen' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subCoordinates-hLen' + (page) + (cnt) + (x)]);
                                            subCoordinatesobj['hLen'] = data['subCoordinates-hLen' + (page) + (cnt) + (x)];
                                            data['subCoordinates-hLen' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subCoordinates-yCoor' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-yCoor' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subCoordinates-yCoor' + (page) + (cnt) + (x)]);
                                            subCoordinatesobj['yCoor'] = data['subCoordinates-yCoor' + (page) + (cnt) + (x)];
                                            data['subCoordinates-yCoor' + (page) + (cnt) + (x)] = '';
                                        }
                                        segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates'] = {
                                            "_attributes": subCoordinatesobj
                                        };
                                    }
                                    if(('subimage-altText' + (page) + (cnt) + (x) in data) == true & data['subimage-altText' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subimage-altText' + (page) + (cnt) + (x)]);
                                        var subimageobj = {};
                                        subimageobj['altText'] = data['subimage-altText' + (page) + (cnt) + (x)];
                                        data['subimage-altText' + (page) + (cnt) + (x)] = '';
                                        segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['image'] = {
                                            "_attributes": subimageobj
                                        };
                                    }
                                    if(('sublistMarks-altText' + (page) + (cnt) + (x) in data) == true & data['sublistMarks-altText' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['sublistMarks-altText' + (page) + (cnt) + (x)]);
                                        var sublistMarksobj = {};
                                        sublistMarksobj['altText'] = data['sublistMarks-altText' + (page) + (cnt) + (x)];
                                        data['sublistMarks-altText' + (page) + (cnt) + (x)] = '';
                                        if(('sublistMarks-type' + (page) + (cnt) + (x) in data) == true & data['sublistMarks-type' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['sublistMarks-type' + (page) + (cnt) + (x)]);
                                            sublistMarksobj['type'] = data['sublistMarks-type' + (page) + (cnt) + (x)];
                                            data['sublistMarks-type' + (page) + (cnt) + (x)] = '';
                                        }
                                        segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['listMarks'] = {
                                            "_attributes": sublistMarksobj
                                        };
                                    }
                                }
                                subseg.push(segmentjson['segmentProperties']['subSegment']);
                            }
                        }
                        segmentjson['segmentProperties']['subSegment'] = subseg;
                        subseg = [];
                    }
                    segment.push(segmentjson);
                }
            }
                pagetypejson['segment'] = segment;
                segment = [];
                pagetype.push(pagetypejson);
            }
            console.log(pagetype);
            var datajson = {
                "pageType": pagetype
            };
            var datafinal = JSON.stringify(datajson);
            var options = {
                compact: true,
                spaces: 4
            }
            var xml = xmljs.json2xml(datafinal, options);
            xml = xml.replace((new RegExp('&amp;', 'g')),'&');
            console.log("\nXML:\n", xml);
    
            var filescount = files.length;
            var fcnt = 1;
            for(var f=0;f<filescount;f++)
            {
                if(data['pageType-name1'] == files[f].split(".")[0]){
                    fcnt += 1;
                }
            }
    
            fs.writeFile(pgdir + '/'+ data['pageType-name1'] +'.v' + fcnt + '.include', xml, function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("Created include file with Name: "+ data['pageType-name1'] +".v" + fcnt + ".include");
            });
            res.redirect('/index/api/pagetypeinclude');
        });
    },
    postEditPageType: function (req, res) {
        var data = req.body;
        console.log(data);
        var template = {};
        var pagetypecount = parseInt(data.pgcount);
        var pgsegcount = parseInt(data.pgsegcount);
        var pgsubsegcount = parseInt(data.pgsubsegcount);
        if(pagetypecount > 0 & data['pageType-name1'] != '')
        {
            var pagetype = [];
            var segment = [];
            var seg = [];
            var subseg = [];
            var pagetypejson = {};
            var segmentjson = {};
            for(var page = 1; page <= pagetypecount; page++) {
                console.log(page);
                console.log(data['pageType-name' + page]);
                var pagetypeobj = {};
                pagetypeobj['name'] = data['pageType-name' + page];
                pagetypejson = {
                    "_attributes": pagetypeobj
                };
                
                for(var cnt = 1; cnt <= pgsegcount; cnt++) {
                    if(('segment-name' + page + cnt in data) == true & data['segment-name' + page + cnt] != '') {
                        console.log(data['segment-name' + page + cnt]);
                        var segmentobj = {};
                        segmentobj['name'] = data['segment-name' + page + cnt];
                        segmentjson = {
                            "_attributes": segmentobj
                        };
                        if(('segmentProperties-type' + page + cnt in data) == true & data['segmentProperties-type' + page + cnt] != '') {
                            console.log(data['segmentProperties-type' + page + cnt]);
                            var segmentPropertiesobj = {};
                            segmentPropertiesobj['type'] = data['segmentProperties-type' + page + cnt];
                            data['segmentProperties-type' + page + cnt] = '';
                            if(('segmentProperties-duplicate' + page + cnt in data) == true & data['segmentProperties-duplicate' + page + cnt] != '') {
                                console.log(data['segmentProperties-duplicate' + page + cnt]);
                                segmentPropertiesobj['duplicate'] = data['segmentProperties-duplicate' + page + cnt];
                                data['segmentProperties-duplicate' + page + cnt] = '';
                            }
                            if(('segmentProperties-oneLineRowTable' + page + cnt in data) == true & data['segmentProperties-oneLineRowTable' + page + cnt] != '') {
                                console.log(data['segmentProperties-oneLineRowTable' + page + cnt]);
                                segmentPropertiesobj['oneLineRowTable'] = data['segmentProperties-oneLineRowTable' + page + cnt];
                                data['segmentProperties-oneLineRowTable' + page + cnt] = '';
                            }
                            if(('segmentProperties-complexTable' + page + cnt in data) == true & data['segmentProperties-complexTable' + page + cnt] != '') {
                                console.log(data['segmentProperties-complexTable' + page + cnt]);
                                segmentPropertiesobj['complexTable'] = data['segmentProperties-complexTable' + page + cnt];
                                data['segmentProperties-complexTable' + page + cnt] = '';
                            }
                            if(('segmentProperties-NeedMergeText' + page + cnt in data) == true & data['segmentProperties-NeedMergeText' + page + cnt] != '') {
                                console.log(data['segmentProperties-NeedMergeText' + page + cnt]);
                                segmentPropertiesobj['NeedMergeText'] = data['segmentProperties-NeedMergeText' + page + cnt];
                                data['segmentProperties-NeedMergeText' + page + cnt] = '';
                            }
                            if(('segmentProperties-phaseADesignTable' + page + cnt in data) == true & data['segmentProperties-phaseADesignTable' + page + cnt] != '') {
                                console.log(data['segmentProperties-phaseADesignTable'] + page + cnt);
                                segmentPropertiesobj['phaseADesignTable'] = data['segmentProperties-phaseADesignTable' + page + cnt];
                                data['segmentProperties-phaseADesignTable' + page + cnt] = '';
                            }
                            if(('segmentProperties-processed' + page + cnt in data) == true & data['segmentProperties-processed' + page + cnt] != '') {
                                console.log(data['segmentProperties-processed' + page + cnt]);
                                segmentPropertiesobj['processed'] = data['segmentProperties-processed' + page + cnt];
                                data['segmentProperties-processed' + page + cnt] = '';
                            }
                            segmentjson['segmentProperties'] = {
                                "_attributes": segmentPropertiesobj
                            };
                            if(('coordinates-column' + page + cnt in data) == true & data['coordinates-column' + page + cnt] != '') {
                                console.log(data['coordinates-column' + page + cnt]);
                                var coordinatesobj = {};
                                coordinatesobj['column'] = data['coordinates-column' + page + cnt];
                                data['coordinates-column' + page + cnt] = '';
                                if(('coordinates-calculate' + page + cnt in data) == true & data['coordinates-calculate' + page + cnt] != '') {
                                    console.log(data['coordinates-calculate' + page + cnt]);
                                    coordinatesobj['calculate'] = data['coordinates-calculate' + page + cnt];
                                    data['coordinates-calculate' + page + cnt] = '';
                                }
                                if(('coordinates-closePhrase' + page + cnt in data) == true & data['coordinates-closePhrase' + page + cnt] != '') {
                                    console.log(data['coordinates-closePhrase' + page + cnt]);
                                    coordinatesobj['closePhrase'] = data['coordinates-closePhrase' + page + cnt];
                                    data['coordinates-closePhrase' + page + cnt] = '';
                                }
                                if(('coordinates-hLen' + page + cnt in data) == true & data['coordinates-hLen' + page + cnt] != '') {
                                    console.log(data['coordinates-hLen' + page + cnt]);
                                    coordinatesobj['hLen'] = data['coordinates-hLen' + page + cnt];
                                    data['coordinates-hLen' + page + cnt] = '';
                                }
                                if(('coordinates-setCoordinates' + page + cnt in data) == true & data['coordinates-setCoordinates' + page + cnt] != '') {
                                    console.log(data['coordinates-setCoordinates' + page + cnt]);
                                    coordinatesobj['setCoordinates'] = data['coordinates-setCoordinates' + page + cnt];
                                    data['coordinates-setCoordinates' + page + cnt] = '';
                                }
                                if(('coordinates-yCoor' + page + cnt in data) == true & data['coordinates-yCoor' + page + cnt] != '') {
                                    console.log(data['coordinates-yCoor' + page + cnt]);
                                    coordinatesobj['yCoor'] = data['coordinates-yCoor' + page + cnt];
                                    data['coordinates-yCoor' + page + cnt] = '';
                                }
                                segmentjson['segmentProperties']['coordinates'] = {
                                    "_attributes": coordinatesobj
                                };
                            }
                            if(('header-active' + page + cnt in data) == true & data['header-active' + page + cnt] != '') {
                                console.log(data['header-active' + page + cnt]);
                                var headerobj = {};
                                headerobj['active'] = data['header-active' + page + cnt];
                                data['header-active' + page + cnt] = '';
                                if(('header-level' + page + cnt in data) == true & data['header-level' + page + cnt] != '') {
                                    console.log(data['header-level' + page + cnt]);
                                    headerobj['level'] = data['header-level' + page + cnt];
                                    data['header-level' + page + cnt] = '';
                                }
                                if(('header-bookmarkText' + page + cnt in data) == true & data['header-bookmarkText' + page + cnt] != '') {
                                    console.log(data['header-bookmarkText' + page + cnt]);
                                    headerobj['bookmarkText'] = data['header-bookmarkText' + page + cnt];
                                    data['header-bookmarkText' + page + cnt] = '';
                                }
                                if(('header-type' + page + cnt in data) == true & data['header-type' + page + cnt] != '') {
                                    console.log(data['header-type' + page + cnt]);
                                    headerobj['type'] = data['header-type' + page + cnt];
                                    data['header-type' + page + cnt] = '';
                                }
                                segmentjson['segmentProperties']['header'] = {
                                    "_attributes": headerobj
                                };
                            }
                            if(('image-altText' + page + cnt in data) == true & data['image-altText' + page + cnt] != '') {
                                console.log(data['image-altText' + page + cnt]);
                                var imageobj = {};
                                imageobj['altText'] = data['image-altText' + page + cnt];
                                data['image-altText' + page + cnt] = '';
                                segmentjson['segmentProperties']['image'] = {
                                    "_attributes": imageobj
                                };
                            }
                            if(('listMarks-altText' + page + cnt in data) == true & data['listMarks-altText' + page + cnt] != '') {
                                console.log(data['listMarks-altText' + page + cnt]);
                                var listMarksobj = {};
                                listMarksobj['altText'] = data['listMarks-altText' + page + cnt];
                                data['listMarks-altText' + page + cnt] = '';
                                if(('listMarks-type' + page + cnt in data) == true & data['listMarks-type' + page + cnt] != '') {
                                    console.log(data['listMarks-type' + page + cnt]);
                                    listMarksobj['type'] = data['listMarks-type' + page + cnt];
                                    data['listMarks-type' + page + cnt] = '';
                                }
                                segmentjson['segmentProperties']['listMarks'] = {
                                    "_attributes": listMarksobj
                                };
                            }
                            for(var x = 1; x <= pgsubsegcount; x++) {
                                if(('subSegment-name' + (page) + (cnt) + (x) in data) == true & data['subSegment-name' + (page) + (cnt) + (x)] != '') {
                                    console.log(data['subSegment-name' + (page) + (cnt) + (x)]);
                                    var subSegmentobj = {};
                                    subSegmentobj['name'] = data['subSegment-name' + (page) + (cnt) + (x)];
                                    data['subSegment-name' + (page) + (cnt) + (x)] = '';
                                    segmentjson['segmentProperties']['subSegment'] = {
                                        "_attributes": subSegmentobj
                                    };
                                    if(('subSegmentProperties-type' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-type' + (page) + (cnt) + (x)] != '') {
                                        console.log(data['subSegmentProperties-type' + (page) + (cnt) + (x)]);
                                        var subSegmentPropertiesobj = {};
                                        subSegmentPropertiesobj['type'] = data['subSegmentProperties-type' + (page) + (cnt) + (x)];
                                        data['subSegmentProperties-type' + (page) + (cnt) + (x)] = '';
                                        if(('subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)]);
                                            subSegmentPropertiesobj['oneLineRowTable'] = data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)];
                                            data['subSegmentProperties-oneLineRowTable' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subSegmentProperties-complexTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)]);
                                            subSegmentPropertiesobj['complexTable'] = data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)];
                                            data['subSegmentProperties-complexTable' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)]);
                                            subSegmentPropertiesobj['NeedMergeText'] = data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)];
                                            data['subSegmentProperties-NeedMergeText' + (page) + (cnt) + (x)] = '';
                                        }
                                        if(('subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x) in data) == true & data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)]);
                                            subSegmentPropertiesobj['phaseADesignTable'] = data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)];
                                            data['subSegmentProperties-phaseADesignTable' + (page) + (cnt) + (x)] = '';
                                        }
                                        segmentjson['segmentProperties']['subSegment']['subSegmentProperties'] = {
                                            "_attributes": subSegmentPropertiesobj
                                        };
                                        if(('subCoordinates-calculate' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-calculate' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subCoordinates-calculate' + (page) + (cnt) + (x)]);
                                            var subCoordinatesobj = {};
                                            subCoordinatesobj['calculate'] = data['subCoordinates-calculate' + (page) + (cnt) + (x)];
                                            data['subCoordinates-calculate' + (page) + (cnt) + (x)] = '';
                                            if(('subCoordinates-column' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-column' + (page) + (cnt) + (x)] != '') {
                                                console.log(data['subCoordinates-column' + (page) + (cnt) + (x)]);
                                                subCoordinatesobj['column'] = data['subCoordinates-column' + (page) + (cnt) + (x)];
                                                data['subCoordinates-column' + (page) + (cnt) + (x)] = '';
                                            }
                                            if(('subCoordinates-closePhrase' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-closePhrase' + (page) + (cnt) + (x)] != '') {
                                                console.log(data['subCoordinates-closePhrase' + (page) + (cnt) + (x)]);
                                                subCoordinatesobj['closePhrase'] = data['subCoordinates-closePhrase' + (page) + (cnt) + (x)];
                                                data['subCoordinates-closePhrase' + (page) + (cnt) + (x)] = '';
                                            }
                                            if(('subCoordinates-hLen' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-hLen' + (page) + (cnt) + (x)] != '') {
                                                console.log(data['subCoordinates-hLen' + (page) + (cnt) + (x)]);
                                                subCoordinatesobj['hLen'] = data['subCoordinates-hLen' + (page) + (cnt) + (x)];
                                                data['subCoordinates-hLen' + (page) + (cnt) + (x)] = '';
                                            }
                                            if(('subCoordinates-yCoor' + (page) + (cnt) + (x) in data) == true & data['subCoordinates-yCoor' + (page) + (cnt) + (x)] != '') {
                                                console.log(data['subCoordinates-yCoor' + (page) + (cnt) + (x)]);
                                                subCoordinatesobj['yCoor'] = data['subCoordinates-yCoor' + (page) + (cnt) + (x)];
                                                data['subCoordinates-yCoor' + (page) + (cnt) + (x)] = '';
                                            }
                                            segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['subCoordinates'] = {
                                                "_attributes": subCoordinatesobj
                                            };
                                        }
                                        if(('subimage-altText' + (page) + (cnt) + (x) in data) == true & data['subimage-altText' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['subimage-altText' + (page) + (cnt) + (x)]);
                                            var subimageobj = {};
                                            subimageobj['altText'] = data['subimage-altText' + (page) + (cnt) + (x)];
                                            data['subimage-altText' + (page) + (cnt) + (x)] = '';
                                            segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['image'] = {
                                                "_attributes": subimageobj
                                            };
                                        }
                                        if(('sublistMarks-altText' + (page) + (cnt) + (x) in data) == true & data['sublistMarks-altText' + (page) + (cnt) + (x)] != '') {
                                            console.log(data['sublistMarks-altText' + (page) + (cnt) + (x)]);
                                            var sublistMarksobj = {};
                                            sublistMarksobj['altText'] = data['sublistMarks-altText' + (page) + (cnt) + (x)];
                                            data['sublistMarks-altText' + (page) + (cnt) + (x)] = '';
                                            if(('sublistMarks-type' + (page) + (cnt) + (x) in data) == true & data['sublistMarks-type' + (page) + (cnt) + (x)] != '') {
                                                console.log(data['sublistMarks-type' + (page) + (cnt) + (x)]);
                                                sublistMarksobj['type'] = data['sublistMarks-type' + (page) + (cnt) + (x)];
                                                data['sublistMarks-type' + (page) + (cnt) + (x)] = '';
                                            }
                                            segmentjson['segmentProperties']['subSegment']['subSegmentProperties']['listMarks'] = {
                                                "_attributes": sublistMarksobj
                                            };
                                        }
                                    }
                                    subseg.push(segmentjson['segmentProperties']['subSegment']);
                                }
                            }
                            segmentjson['segmentProperties']['subSegment'] = subseg;
                            subseg = [];
                        }
    
                        segment.push(segmentjson);
    
                        if(('segment' + page + cnt in data) == true & typeof data['segment' + page + cnt] == 'object')
                        {
                            for(var i=0;i<data['segment' + page + cnt].length;i++)
                            {
                                if(data['segment' + page + cnt][i] != '') {
                                    console.log(data['segment' + page + cnt][i]);
                                    var s = '\n\t\t&' + data['segment' + page + cnt][i].split(".")[0] + ';';
                                    seg.push(s);
                                    data['segment' + page + cnt][i] = '';
                                }
                            }
                        }
                        else
                        {
                            if(('segment' + page + cnt in data) == true & data['segment' + page + cnt] != '') {
                                console.log(data['segment' + page + cnt]);
                                var s = '\n\t\t&' + data['segment' + page + cnt].split(".")[0] + ';';
                                seg.push(s);
                            }
                        }
                    }
                }
                pagetypejson['segment'] = segment;
                if(('segment' + 1 + 1 in data) == true & data['segment' + 1 + 1] != '') {
                pagetypejson['_text'] = seg
                seg = [];
                }
                segment = [];
                pagetype.push(pagetypejson);
            }
            console.log(pagetype);
            template['pageType'] = pagetype;
    
            var datajson = {"pageType": template['pageType']};
            console.log(datajson);
            var datafinal = JSON.stringify(datajson);
            var options = {
                compact: true,
                spaces: 4
            }
            var xml = xmljs.json2xml(datafinal, options);
            xml = xml.replace((new RegExp('&amp;', 'g')),'&');
            console.log("\nXML:\n", xml);
            fs.unlink(pgdir + '/' + data['orgfilename'], (err) => {
                if (err) throw err;
            });
            fs.writeFile(pgdir + '/'+ data['filename'], xml, function(err) {
                if(err) {
                    return console.log(err);
                }
                console.log("Updated file with Name: "+ data['filename']);
            });
            res.redirect('/index/api/pagetypeinclude');
        }
    },
    postImportPageType: function (req, res) {
        res.redirect('/api/pagetypeinclude');
    }
};

module.exports = pagetypeController;