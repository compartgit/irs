const conn = require('../database');

const User = {
    UserInfo: function (email, callback) {
        var sql='SELECT * FROM users WHERE email = ?';
        conn.query(sql, [email], callback);
    },
    InsertUser: function (inputData, callback) {
        var sql = 'INSERT INTO users SET ?';
        conn.query(sql, inputData, callback);
    },
    UpdatePassword: function (password, email, callback) {
        var sql='UPDATE users SET password = ? WHERE email = ?';
        conn.query(sql, [password, email], callback);
    },
    UpdateUser: function (name, email, orgemail, callback) {
        var sql = 'UPDATE users SET name = ?, email = ? WHERE email = ?';
        conn.query(sql, [name,email,orgemail], callback);
    },
    UpdateUser1: function (name, email, password, orgemail, callback) {
        var sql = 'UPDATE users SET name = ?, email = ?, password = ? WHERE email = ?';
        conn.query(sql, [name,email,password,orgemail], callback);
    }
}

module.exports = User;