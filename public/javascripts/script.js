var pg = 1;
function addTempPageType(input) {
    cnt += 1;
    pg += 1;
    $.ajax({
        url: "/index/api/getfilenames",
        success: function(result) {
        var segment1 = [];
        for(var i in result.segment) {
            segment1.push({
                title: result["segment"][i],
                label: result["segment"][i],
                value: result["segment"][i]
            });
        }
        $("#selsegment"+pg+cnt).multiselect('dataprovider', segment1);
        
    }});
    
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="pagetype${pg}" >
                            <fieldset class="border p-2 form-group">
                            <legend id="pgtype${pg}" data-bs-toggle="collapse" href="#pgtypecol${pg}" data-bs-target="#pgtypecol${pg}" role="button" aria-expanded="true" aria-controls="pgtypecol${pg}">Page Type <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="pgtypecol${pg}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label class="required-field" for="pageType-name${pg}">Name:</label>
                                <select class="pagetypecount form-select" id="pageType-name${pg}" name="pageType-name${pg}" required>
                                <option value></option>    
                                <option  value="standardHeader">standardHeader</option>
                                    <option  value="continuationHeader">continuationHeader</option>
                                    <option  value="stdFooter">stdFooter</option>
                                    <option  value="body">body</option>
                                    <option value="label">label</option>
                                </select>
                                </div>
                                <br>
                                <div id="segment${pg}${cnt}" class="segment${pg}${cnt}">
                            <h2 >Segment</h2>
                        <label class="fontstyle" >Segments are a logical groupings of text most of the time with a header in a pageType.</label><br><br>
                        <fieldset class="border p-2 form-group">
                        <legend id="seg${pg}${cnt}" data-bs-toggle="collapse" href="#segmentcol${pg}${cnt}" data-bs-target="#segmentcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segmentcol${pg}${cnt}">Segment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                        <div id="segmentcol${pg}${cnt}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="segment-name${pg}${cnt}">Name:</label>
                            <input class="pgsegmentcount form-control" type="text" id="segment-name${pg}${cnt}" name="segment-name${pg}${cnt}" required>
                            <label  class="fontstyle" for="segment-name${pg}${cnt}">Segment name is the text used to find the Y coordinate(top) of the segment.</label>
                            </div><br>
                            <h2 >Segment Properties</h2>
                            <label  class="fontstyle">SegmentProperties are the settings used to capture the a segment correctly with the type of segment, coordinates calculation settings, and the header information.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="segprop${pg}${cnt}" data-bs-toggle="collapse" href="#segpropcol${pg}${cnt}" data-bs-target="#segpropcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segpropcol${pg}${cnt}">Segment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="segpropcol${pg}${cnt}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label  for="segmentProperties-type${pg}${cnt}">Type:</label>
                                <select class="form-select" id="segmentProperties-type${pg}${cnt}" name="segmentProperties-type${pg}${cnt}">
                                <option value></option>    
                                <option  value="combo">combo</option>
                                    <option  value="paragraph">paragraph</option>
                                    <option  value="image">image</option>
                                    <option  value="lines">lines</option>
                                    <option  value="table">table</option>
                                </select><br><br>
                                    <label  for="segmentProperties-duplicate${pg}${cnt}" >Duplicate:</label>
                                    <select class="form-select" id="segmentProperties-duplicate${pg}${cnt}" name="segmentProperties-duplicate${pg}${cnt}" >
										<option value selected>false</option>
										<option value="true">true</option>
									</select>
                                    <label  class="fontstyle" for="segmentProperties-duplicate${pg}${cnt}">The duplicate attribute is used segment used the same beginning for a segment. Usually happens with a segmemnt type table.</label><br>
                                    <input id="seg_advanced_btn${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
									<div id="seg_advanced${pg}${cnt}">
                                    <br>
                                    <label  for="segmentProperties-oneLineRowTable${pg}${cnt}" >oneLineRowTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-oneLineRowTable${pg}${cnt}" name="segmentProperties-oneLineRowTable${pg}${cnt}" value="false">
                                    <label  class="fontstyle" for="segmentProperties-oneLineRowTable${pg}${cnt}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                                    <label  for="segmentProperties-complexTable${pg}${cnt}" >complexTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-complexTable${pg}${cnt}" name="segmentProperties-complexTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-complexTable${pg}${cnt}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                                    <label  for="segmentProperties-NeedMergeText${pg}${cnt}" >NeedMergeText:</label>
                                    <input class="form-control" type="text" id="segmentProperties-NeedMergeText${pg}${cnt}" name="segmentProperties-NeedMergeText${pg}${cnt}" value="false">
                                    <label  class="fontstyle" for="segmentProperties-NeedMergeText${pg}${cnt}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                                    <label  for="segmentProperties-phaseADesignTable${pg}${cnt}" >phaseADesignTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-phaseADesignTable${pg}${cnt}" name="segmentProperties-phaseADesignTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-phaseADesignTable${pg}${cnt}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label><br><br>
                                    <label  for="segmentProperties-processed${pg}${cnt}" >processed:</label>
                                    <input class="form-control" type="text" id="segmentProperties-processed${pg}${cnt}" name="segmentProperties-processed${pg}${cnt}" value="no" readonly>
                                    <label class="fontstyle"  for="segmentProperties-processed${pg}${cnt}">The processed attribute is used internally in the app. It doesn't not need to exist in the individual templates.</label></div></div><br>
                                        <input id="addCoor${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Coordinates" onclick="addPgCoordinates(this)">
                                        <input id="addHeader${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Header" onclick="addPgHeader(this)">
                                        <input id="addImage${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addPgImage(this)">
                                        <input id="addListMarks${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add ListMarks" onclick="addPgListMarks(this)">
                                        <input id="addSubSeg${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add SubSegment" onclick="addPgSubSegment(this)"><br>
                                    </div>
                                    </fieldset>
                                    </div>
                                </fieldset>
                                <input class="btn btn-black btn-sm" type="button" value="Add Segment" onclick="addPgSegment(this)">
                                <select id="selsegment${pg}${cnt}" name="segment${pg}${cnt}" multiple> </select>
                                </div></div>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePageType(this)"><br><br>
                        </div>`
    );

    $('#pgtype' + pg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#seg' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    
    $('#segprop' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#selsegment'+pg+cnt).multiselect({
        nonSelectedText: 'Include Segment Inside PageType',
        includeSelectAllOption: true,
        buttonClass: 'btn btn-black btn-sm',
        numberDisplayed: 1,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200
    });

    $('#addCoor' + pg + cnt).hide();
	$('#addHeader' + pg + cnt).hide();
	$('#addImage' + pg + cnt).hide();
	$('#addListMarks' + pg + cnt).hide();
	$('#addSubSeg' + pg + cnt).hide();

	$('#segmentProperties-type' + pg + cnt).change(function(){
		console.log($(this).val());
		var type = $(this).val();
		if(type == 'paragraph')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'combo')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
			$('#addSubSeg' + pg + cnt).show();
		}
		else if(type == 'table')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'lines')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'image')
		{
			$('#addCoor' + pg + cnt).hide();
			$('#addHeader' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addImage' + pg + cnt).show();
		}
        else
        {
            $('#addCoor' + pg + cnt).hide();
            $('#addHeader' + pg + cnt).hide();
            $('#addImage' + pg + cnt).hide();
            $('#addListMarks' + pg + cnt).hide();
            $('#addSubSeg' + pg + cnt).hide();
        }
    });

    $('#seg_advanced' + pg + cnt).hide();
	$("#seg_advanced_btn" + pg + cnt).click(function() {
		$('#seg_advanced' + pg + cnt).toggle();
	});

}

var pg = 1;
function addPageType(input) {
    cnt += 1;
    pg += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="pagetype${pg}" >
                            <fieldset class="border p-2 form-group">
                                <legend id="pgtype${pg}" data-bs-toggle="collapse" href="#pgtypecol${pg}" data-bs-target="#pgtypecol${pg}" role="button" aria-expanded="true" aria-controls="pgtypecol${pg}">Page Type <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="pgtypecol${pg}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label class="required-field" for="pageType-name${pg}">Name:</label>
                                <select class="pagetypecount form-select" id="pageType-name${pg}" name="pageType-name${pg}" required>
                                <option value></option>    
                                <option  value="standardHeader">standardHeader</option>
                                    <option  value="continuationHeader">continuationHeader</option>
                                    <option  value="stdFooter">stdFooter</option>
                                    <option  value="body">body</option>
                                    <option value="label">label</option>
                                </select></div><br>
                                <div id="segment${pg}${cnt}" class="segment${pg}${cnt}">
                            <h2 >Segment</h2>
                        <label class="fontstyle" >Segments are a logical groupings of text most of the time with a header in a pageType.</label><br><br>
                        <fieldset class="border p-2 form-group">
                            <legend id="seg${pg}${cnt}" data-bs-toggle="collapse" href="#segmentcol${pg}${cnt}" data-bs-target="#segmentcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segmentcol${pg}${cnt}">Segment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
							<div id="segmentcol${pg}${cnt}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="segment-name${pg}${cnt}">Name:</label>
                            <input class="pgsegmentcount form-control" type="text" id="segment-name${pg}${cnt}" name="segment-name${pg}${cnt}" required>
                            <label class="fontstyle"  for="segment-name${pg}${cnt}">Segment name is the text used to find the Y coordinate(top) of the segment.</label></div><br>
                            <h2 >Segment Properties</h2>
                            <label class="fontstyle" >SegmentProperties are the settings used to capture the a segment correctly with the type of segment, coordinates calculation settings, and the header information.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="segprop${pg}${cnt}" data-bs-toggle="collapse" href="#segpropcol${pg}${cnt}" data-bs-target="#segpropcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segpropcol${pg}${cnt}">Segment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
								<div id="segpropcol${pg}${cnt}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label  for="segmentProperties-type${pg}${cnt}">Type:</label>
                                <select class="form-select" id="segmentProperties-type${pg}${cnt}" name="segmentProperties-type${pg}${cnt}">
                                <option value></option>    
                                <option  value="combo">combo</option>
                                    <option  value="paragraph">paragraph</option>
                                    <option  value="image">image</option>
                                    <option  value="lines">lines</option>
                                    <option  value="table">table</option>
                                </select><br><br>
                                    <label  for="segmentProperties-duplicate${pg}${cnt}">Duplicate:</label>
                                    <select class="form-select" id="segmentProperties-duplicate${pg}${cnt}" name="segmentProperties-duplicate${pg}${cnt}" >
										<option value selected>false</option>
										<option value="true">true</option>
									</select>
                                    <label class="fontstyle"  for="segmentProperties-duplicate${pg}${cnt}">The duplicate attribute is used segment used the same beginning for a segment. Usually happens with a segmemnt type table.</label><br>
                                    <input id="seg_advanced_btn${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
									<div id="seg_advanced${pg}${cnt}">
                                    <br>
                                    <label  for="segmentProperties-oneLineRowTable${pg}${cnt}">oneLineRowTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-oneLineRowTable${pg}${cnt}" name="segmentProperties-oneLineRowTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-oneLineRowTable${pg}${cnt}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                                    <label  for="segmentProperties-complexTable${pg}${cnt}">complexTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-complexTable${pg}${cnt}" name="segmentProperties-complexTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-complexTable${pg}${cnt}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                                    <label  for="segmentProperties-NeedMergeText${pg}${cnt}">NeedMergeText:</label>
                                    <input class="form-control" type="text" id="segmentProperties-NeedMergeText${pg}${cnt}" name="segmentProperties-NeedMergeText${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-NeedMergeText${pg}${cnt}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                                    <label  for="segmentProperties-phaseADesignTable${pg}${cnt}">phaseADesignTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-phaseADesignTable${pg}${cnt}" name="segmentProperties-phaseADesignTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-phaseADesignTable${pg}${cnt}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label><br><br>
                                    <label  for="segmentProperties-processed${pg}${cnt}">processed:</label>
                                    <input class="form-control" type="text" id="segmentProperties-processed${pg}${cnt}" name="segmentProperties-processed${pg}${cnt}" value="no" readonly>
                                    <label class="fontstyle"  for="segmentProperties-processed${pg}${cnt}">The processed attribute is used internally in the app. It doesn't not need to exist in the individual templates.</label></div></div><br>
                                        <input id="addCoor${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Coordinates" onclick="addPgCoordinates(this)">
                                        <input id="addHeader${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Header" onclick="addPgHeader(this)">
                                        <input id="addImage${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addPgImage(this)">
                                        <input id="addListMarks${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add ListMarks" onclick="addPgListMarks(this)">
                                        <input id="addSubSeg${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add SubSegment" onclick="addPgSubSegment(this)"><br>
                                    </div>
                                    </fieldset>
                                    </div>
                                </fieldset>
                                <input class="btn btn-black btn-sm" type="button" value="Add Segment" onclick="addPgSegment(this)">
                                </div>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePageType(this)"><br><br>
                        </div>`
    );

    $('#pgtype' + pg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#seg' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    
    $('#segprop' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#addCoor' + pg + cnt).hide();
	$('#addHeader' + pg + cnt).hide();
	$('#addImage' + pg + cnt).hide();
	$('#addListMarks' + pg + cnt).hide();
	$('#addSubSeg' + pg + cnt).hide();

	$('#segmentProperties-type' + pg + cnt).change(function(){
		console.log($(this).val());
		var type = $(this).val();
		if(type == 'paragraph')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'combo')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
			$('#addSubSeg' + pg + cnt).show();
		}
		else if(type == 'table')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'lines')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'image')
		{
			$('#addCoor' + pg + cnt).hide();
			$('#addHeader' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addImage' + pg + cnt).show();
		}
        else
		{
			$('#addCoor' + pg + cnt).hide();
            $('#addHeader' + pg + cnt).hide();
            $('#addImage' + pg + cnt).hide();
            $('#addListMarks' + pg + cnt).hide();
            $('#addSubSeg' + pg + cnt).hide();
		}
    });

    $('#seg_advanced' + pg + cnt).hide();
	$("#seg_advanced_btn" + pg + cnt).click(function() {
		$('#seg_advanced' + pg + cnt).toggle();
	});
}

var cnt = 1;
function addPgSegment(input) {
    cnt += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="segment${pg}${cnt}" class="segment${pg}${cnt}">
                        <fieldset class="border p-2 form-group">
                        <legend id="seg${pg}${cnt}" data-bs-toggle="collapse" href="#segmentcol${pg}${cnt}" data-bs-target="#segmentcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segmentcol${pg}${cnt}">Segment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                        <div id="segmentcol${pg}${cnt}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="segment-name${pg}${cnt}">Name:</label>
                            <input class="pgsegmentcount form-control" type="text" id="segment-name${pg}${cnt}" name="segment-name${pg}${cnt}" required>
                            <label class="fontstyle"  for="segment-name${pg}${cnt}">Segment name is the text used to find the Y coordinate(top) of the segment.</label></div><br>
                            <h2 >Segment Properties</h2>
                            <label class="fontstyle" >SegmentProperties are the settings used to capture the a segment correctly with the type of segment, coordinates calculation settings, and the header information.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="segprop${pg}${cnt}" data-bs-toggle="collapse" href="#segpropcol${pg}${cnt}" data-bs-target="#segpropcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="segpropcol${pg}${cnt}">Segment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="segpropcol${pg}${cnt}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label  for="segmentProperties-type${pg}${cnt}">Type:</label>
                                <select class="form-select" id="segmentProperties-type${pg}${cnt}" name="segmentProperties-type${pg}${cnt}">
                                <option value></option>    
                                <option  value="combo">combo</option>
                                    <option  value="paragraph">paragraph</option>
                                    <option  value="image">image</option>
                                    <option  value="lines">lines</option>
                                    <option  value="table">table</option>
                                </select><br><br>
                                    <label  for="segmentProperties-duplicate${pg}${cnt}" >Duplicate:</label>
                                    <select class="form-select" id="segmentProperties-duplicate${pg}${cnt}" name="segmentProperties-duplicate${pg}${cnt}" >
										<option value selected>false</option>
										<option value="true">true</option>
									</select>
                                    <label class="fontstyle"  for="segmentProperties-duplicate${pg}${cnt}">The duplicate attribute is used segment used the same beginning for a segment. Usually happens with a segmemnt type table.</label><br>
                                    <input id="seg_advanced_btn${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
									<div id="seg_advanced${pg}${cnt}">
                                    <br>
                                    <label  for="segmentProperties-oneLineRowTable${pg}${cnt}" >oneLineRowTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-oneLineRowTable${pg}${cnt}" name="segmentProperties-oneLineRowTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-oneLineRowTable${pg}${cnt}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                                    <label  for="segmentProperties-complexTable${pg}${cnt}">complexTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-complexTable${pg}${cnt}" name="segmentProperties-complexTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-complexTable${pg}${cnt}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                                    <label  for="segmentProperties-NeedMergeText${pg}${cnt}">NeedMergeText:</label>
                                    <input class="form-control" type="text" id="segmentProperties-NeedMergeText${pg}${cnt}" name="segmentProperties-NeedMergeText${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-NeedMergeText${pg}${cnt}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                                    <label  for="segmentProperties-phaseADesignTable${pg}${cnt}">phaseADesignTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-phaseADesignTable${pg}${cnt}" name="segmentProperties-phaseADesignTable${pg}${cnt}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-phaseADesignTable${pg}${cnt}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label><br><br>
                                    <label  for="segmentProperties-processed${pg}${cnt}" >processed:</label>
                                    <input class="form-control" type="text" id="segmentProperties-processed${pg}${cnt}" name="segmentProperties-processed${pg}${cnt}" value="no" readonly>
                                    <label class="fontstyle"  for="segmentProperties-processed${pg}${cnt}">The processed attribute is used internally in the app. It doesn't not need to exist in the individual templates.</label></div></div><br>
                                    <input id="addCoor${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Coordinates" onclick="addPgCoordinates(this)">
                                    <input id="addHeader${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Header" onclick="addPgHeader(this)">
                                    <input id="addImage${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addPgImage(this)">
                                    <input id="addListMarks${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add ListMarks" onclick="addPgListMarks(this)">
                                    <input id="addSubSeg${pg}${cnt}" class="btn btn-black btn-sm" type="button" value="Add SubSegment" onclick="addPgSubSegment(this)">
                                    <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePgSeg(this)"><br><br>
                                </div>
                            </fieldset>
                            </div>
                        </fieldset>
                        </div>`
    );

    $('#seg' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    
    $('#segprop' + pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#addCoor' + pg + cnt).hide();
	$('#addHeader' + pg + cnt).hide();
	$('#addImage' + pg + cnt).hide();
	$('#addListMarks' + pg + cnt).hide();
	$('#addSubSeg' + pg + cnt).hide();

	$('#segmentProperties-type' + pg + cnt).change(function(){
		console.log($(this).val());
		var type = $(this).val();
		if(type == 'paragraph')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'combo')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
			$('#addSubSeg' + pg + cnt).show();
		}
		else if(type == 'table')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'lines')
		{
			$('#addImage' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addCoor' + pg + cnt).show();
			$('#addHeader' + pg + cnt).show();
		}
		else if(type == 'image')
		{
			$('#addCoor' + pg + cnt).hide();
			$('#addHeader' + pg + cnt).hide();
			$('#addListMarks' + pg + cnt).hide();
			$('#addSubSeg' + pg + cnt).hide();
			$('#addImage' + pg + cnt).show();
		}
        else
		{
			$('#addCoor' + pg + cnt).hide();
            $('#addHeader' + pg + cnt).hide();
            $('#addImage' + pg + cnt).hide();
            $('#addListMarks' + pg + cnt).hide();
            $('#addSubSeg' + pg + cnt).hide();
		}
    });

    $('#seg_advanced' + pg + cnt).hide();
	$("#seg_advanced_btn" + pg + cnt).click(function() {
		$('#seg_advanced' + pg + cnt).toggle();
	});
}

var pgsubseg = 0;
function addPgSubSegment(input) {
    pgsubseg += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="subsegment${pg}${cnt}${pgsubseg}" class="subsegment${pg}${cnt}${pgsubseg}">
                            <h2 >SubSegment</h2>
                            <label class="fontstyle" >subSegment(s) is the element that is used with a combo segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="subseg${pg}${cnt}${pgsubseg}" data-bs-toggle="collapse" href="#subsegcol${pg}${cnt}${pgsubseg}" data-bs-target="#subsegcol${pg}${cnt}${pgsubseg}" role="button" aria-expanded="true" aria-controls="subsegcol${pg}${cnt}${pgsubseg}">SubSegment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="subsegcol${pg}${cnt}${pgsubseg}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="subSegment-name${pg}${cnt}${pgsubseg}">name:</label>
                            <input class="pgsubsegmentcount form-control" type="text" id="subSegment-name${pg}${cnt}${pgsubseg}" name="subSegment-name${pg}${cnt}${pgsubseg}" required>
                            <label class="fontstyle"  for="subSegment-name${pg}${cnt}${pgsubseg}">Attribute name is the text used to find the Y coordinate(top) of the subSegment.</label></div><br>
                            <h2 >SubSegment Properties</h2>
                            <label class="fontstyle" >The properties to capture the text block of the subSegment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="subsegprop${pg}${cnt}${pgsubseg}" data-bs-toggle="collapse" href="#subsegpropcol${pg}${cnt}${pgsubseg}" data-bs-target="#subsegpropcol${pg}${cnt}${pgsubseg}" role="button" aria-expanded="true" aria-controls="subsegpropcol${pg}${cnt}${pgsubseg}">SubSegment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="subsegpropcol${pg}${cnt}${pgsubseg}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="subSegmentProperties-type${pg}${cnt}${pgsubseg}">type:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-type${pg}${cnt}${pgsubseg}" name="subSegmentProperties-type${pg}${cnt}${pgsubseg}" required><br>
                            <input id="subseg_advanced_btn${pg}${cnt}${pgsubseg}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
							<div id="subseg_advanced${pg}${cnt}${pgsubseg}">
                            <br/>
                            <label  for="subSegmentProperties-oneLineRowTable${pg}${cnt}${pgsubseg}">oneLineRowTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-oneLineRowTable${pg}${cnt}${pgsubseg}" name="subSegmentProperties-oneLineRowTable${pg}${cnt}${pgsubseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-oneLineRowTable${pg}${cnt}${pgsubseg}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                            <label for="subSegmentProperties-complexTable${pg}${cnt}${pgsubseg}" >complexTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-complexTable${pg}${cnt}${pgsubseg}" name="subSegmentProperties-complexTable${pg}${cnt}${pgsubseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-complexTable${pg}${cnt}${pgsubseg}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                            <label  for="subSegmentProperties-NeedMergeText${pg}${cnt}${pgsubseg}" >NeedMergeText:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-NeedMergeText${pg}${cnt}${pgsubseg}" name="subSegmentProperties-NeedMergeText${pg}${cnt}${pgsubseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-NeedMergeText${pg}${cnt}${pgsubseg}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                            <label  for="subSegmentProperties-phaseADesignTable${pg}${cnt}${pgsubseg}" >phaseADesignTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-phaseADesignTable${pg}${cnt}${pgsubseg}" name="subSegmentProperties-phaseADesignTable${pg}${cnt}${pgsubseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-phaseADesignTable${pg}${cnt}${pgsubseg}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label></div></div><br>
                            <input class="btn btn-black btn-sm" type="button" value="Add SubCoordinates" onclick="addPgSubCoordinates(this)">
                            <input class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addPgSubImage(this)">
                            
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePgSubSeg(this)"><br><br>
                        </div></div>
                        </fieldset>
                            </fieldset>
                            </div>`
    );
    $('#subseg' + pg + cnt + pgsubseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    
    $('#subsegprop' + pg + cnt + pgsubseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#subseg_advanced' + pg + cnt + pgsubseg).hide();
	$("#subseg_advanced_btn" + pg + cnt + pgsubseg).click(function() {
		$('#subseg_advanced' + pg + cnt + pgsubseg).toggle();
	});

}

var pgcoorid = 0;

function addPgCoordinates(input) {
    pgcoorid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="coordinates${pg}${cnt}${pgcoorid}">
                            <h2 >Coordinates</h2>
                            <label class="fontstyle" >The variables used in the coordinates section calculate the x, y, w, and h of the segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="coor${pg}${cnt}" data-bs-toggle="collapse" href="#coorcol${pg}${cnt}" data-bs-target="#coorcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="coorcol${pg}${cnt}">Coordinates <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="coorcol${pg}${cnt}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label  for="coordinates-calculate${pg}${cnt}">calculate:</label>
                                <input class="form-control" type="text" id="coordinates-calculate${pg}${cnt}" name="coordinates-calculate${pg}${cnt}">
                                <label class="fontstyle"  for="coordinates-calculate${pg}${cnt}">calculate attribute is set to true when the coordinates have to caclulated for the coordinates.</label><br><br>
                                <label  for="coordinates-closePhrase${pg}${cnt}">closePhrase:</label>
                                <input class="form-control" type="text" id="coordinates-closePhrase${pg}${cnt}" name="coordinates-closePhrase${pg}${cnt}">
                                <label class="fontstyle"  for="coordinates-closePhrase${pg}${cnt}">closePhrase attribute is set to the ending text or LINE. If there mutliple closePhrase they are split by "%".</label><br><br>
                                <label class="required-field" for="coordinates-column${pg}${cnt}">column:</label>
                                <input class="form-control" type="text" id="coordinates-column${pg}${cnt}" name="coordinates-column${pg}${cnt}" required>
                                <label class="fontstyle"  for="coordinates-column${pg}${cnt}">column is used to provide the xCoor and wLen for the segment.</label><br><br>
                                <label  for="coordinates-hLen${pg}${cnt}" >hLen:</label>
                                <input class="form-control" type="text" id="coordinates-hLen${pg}${cnt}" name="coordinates-hLen${pg}${cnt}">
                                <label class="fontstyle"  for="coordinates-hLen${pg}${cnt}">hLen for the segment. For a combo the hLen is used for the height of the header.</label><br><br>
                                <label  for="coordinates-setCoordinates${pg}${cnt}" >setCoordinates:</label>
                                <input class="form-control" type="text" id="coordinates-setCoordinates${pg}${cnt}" name="coordinates-setCoordinates${pg}${cnt}">
                                <label class="fontstyle"  for="coordinates-setCoordinates${pg}${cnt}">setCoordinates for the segment is an obsolete variable.</label><br><br>
                                <label  for="coordinates-yCoor${pg}${cnt}">yCoor:</label>
                                <input class="form-control" type="text" id="coordinates-yCoor${pg}${cnt}" name="coordinates-yCoor${pg}${cnt}">
                                <label class="fontstyle"  for="coordinates-yCoor${pg}${cnt}">yCoor for the segment.</label></div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#coor'+ pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pgheadid = 0;

function addPgHeader(input) {
    pgheadid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="header${pg}${cnt}${pgheadid}">
                            <h2 >Header</h2>
                            <label class="fontstyle" >The variables used in the header determine the header type of the segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="head${pg}${cnt}" data-bs-toggle="collapse" href="#headcol${pg}${cnt}" data-bs-target="#headcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="headcol${pg}${cnt}">Header <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="headcol${pg}${cnt}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="header-active${pg}${cnt}">active:</label>
                                <input class="form-control" type="text" id="header-active${pg}${cnt}" name="header-active${pg}${cnt}" required>
                                <label class="fontstyle"  for="header-active${pg}${cnt}">active is set to true if a header of exists in the segment.</label><br><br>
                                <label  for="header-bookmarkText${pg}${cnt}">bookmarkText:</label>
                                <input class="form-control" type="text" id="header-bookmarkText${pg}${cnt}" name="header-bookmarkText${pg}${cnt}">
                                <label class="fontstyle"  for="header-bookmarkText${pg}${cnt}">bookmarkText attribute is needed when the header contains a M-dash or a "'". They will not show up correction in the bookMark in Adobe.</label><br><br>
                                <label for="header-level${pg}${cnt}">level:</label>
                                <select class="form-select" id="header-level${pg}${cnt}" name="header-level${pg}${cnt}">
                                <option value></option>    
                                <option  value="H1">H1</option>
                                    <option  value="H2">H2</option>
                                    <option  value="H3">H3</option>
                                    <option  value="H4">H4</option>
                                </select><br>
                                <label  for="header-type${pg}${cnt}">type:</label><br>
                                <select class="form-select" id="header-type${pg}${cnt}" name="header-type${pg}${cnt}">
                                <option value></option>   
                                <option  value="top">top</option>
                                    <option  value="topleft">topleft</option>
                                    <option  value="left">left</option>
                                    <option  value="first">first</option>
                                    <option  value="notice">notice</option>
                                </select><br>
                                </div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#head'+ pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pgimgid = 0;

function addPgImage(input) {
    pgimgid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="image${pg}${cnt}${pgimgid}">
                            <h2 >Image</h2>
                            <label class="fontstyle" >The image element is used for the need of taking an Image on the page that will not be an Artifact and can enter the Alternative Text for the image in here.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="img${pg}${cnt}" data-bs-toggle="collapse" href="#imgcol${pg}${cnt}" data-bs-target="#imgcol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="imgcol${pg}${cnt}">Image <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="imgcol${pg}${cnt}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="image-altText${pg}${cnt}">altText:</label>
                                <input class="form-control" type="text" id="image-altText${pg}${cnt}" name="image-altText${pg}${cnt}" required>
                                <label class="fontstyle"  for="image-altText${pg}${cnt}">The altText attribute is used for entering the altermative text for an image.</label></div><br>
                            </fieldset>    
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#img'+ pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pglistid = 0;

function addPgListMarks(input) {
    pglistid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="listMarks${pg}${cnt}${pglistid}">
                            <h2 >ListMarks</h2>
                                <label class="fontstyle" >The listMarks is the element where an image is used for a book mark.</label><br><br>
                                <fieldset class="border p-2 form-group">
                                <legend id="lstmarks${pg}${cnt}" data-bs-toggle="collapse" href="#lstmarkscol${pg}${cnt}" data-bs-target="#lstmarkscol${pg}${cnt}" role="button" aria-expanded="true" aria-controls="lstmarkscol${pg}${cnt}">ListMarks <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="lstmarkscol${pg}${cnt}" class="form-group col-md-4 col-sm-8 collapse show">
                                    <label class="required-field" for="listMarks-altText${pg}${cnt}">altText:</label>
                                    <input class="form-control" type="text" id="listMarks-altText${pg}${cnt}" name="listMarks-altText${pg}${cnt}" required>
                                    <label class="fontstyle"  for="listMarks-altText${pg}${cnt}">The altText attribute is for the alternative text for the listMark if an image is used.</label><br><br>
                                    <label class="required-field" for="listMarks-type${pg}${cnt}">type:</label>
                                    <input class="form-control" type="text" id="listMarks-type${pg}${cnt}" name="listMarks-type${pg}${cnt}" required>
                                    <label class="fontstyle"  for="listMarks-type${pg}${cnt}">The type attribute is used to set the type of list, e.i. deciamal, roman,...etc.  For future use.</label></div><br>
                                </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#lstmarks'+ pg + cnt).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pgsubcoorid = 0;

function addPgSubCoordinates(input) {
    pgsubcoorid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="subcoordinates${pg}${cnt}${pgsubseg}${pgsubcoorid}">
                            <h2 >SubCoordinates</h2>
                            <label class="fontstyle" >The variables used in the coordinates section calculate the x, y, w, and h of the subSegment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="subcoor${pg}${cnt}${pgsubseg}" data-bs-toggle="collapse" href="#subcoorcol${pg}${cnt}${pgsubseg}" data-bs-target="#subcoorcol${pg}${cnt}${pgsubseg}" role="button" aria-expanded="true" aria-controls="subcoorcol${pg}${cnt}${pgsubseg}">SubCoordinates <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="subcoorcol${pg}${cnt}${pgsubseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="subCoordinates-calculate${pg}${cnt}${pgsubseg}" >calculate:</label>
                                <input class="form-control" type="text" id="subCoordinates-calculate${pg}${cnt}${pgsubseg}" name="subCoordinates-calculate${pg}${cnt}${pgsubseg}" required>
                                <label class="fontstyle"  for="subCoordinates-calculate${pg}${cnt}${pgsubseg}">The variables used in the subCoordinates section calculate the x, y, w, and h of the segment.</label><br><br>
                                <label  for="subCoordinates-closePhrase${pg}${cnt}${pgsubseg}" >closePhrase:</label>
                                <input class="form-control" type="text" id="subCoordinates-closePhrase${pg}${cnt}${pgsubseg}" name="subCoordinates-closePhrase${pg}${cnt}${pgsubseg}"><br>
                                <label class="fontstyle"  for="subCoordinates-closePhrase${pg}${cnt}${pgsubseg}">closePhrase attribute is set to the ending text or LINE. If there mutliple closePhrase they are split by "%".</label><br><br>
                                <label class="required-field" for="subCoordinates-column${pg}${cnt}${pgsubseg}">column:</label>
                                <input class="form-control" type="text" id="subCoordinates-column${pg}${cnt}${pgsubseg}" name="subCoordinates-column${pg}${cnt}${pgsubseg}" required>
                                <label class="fontstyle"  for="subCoordinates-column${pg}${cnt}${pgsubseg}">column is used to provide the xCoor and wLen for the subSegment.</label><br><br>
                                <label  for="subCoordinates-hLen${pg}${cnt}${pgsubseg}" >hLen:</label>
                                <input class="form-control" type="text" id="subCoordinates-hLen${pg}${cnt}${pgsubseg}" name="subCoordinates-hLen${pg}${cnt}${pgsubseg}">
                                <label class="fontstyle"  for="subCoordinates-hLen${pg}${cnt}${pgsubseg}">hLen for the subSegment. For a combo the hLen is used for the height of the header.</label><br><br>
                                <label  for="subCoordinates-yCoor${pg}${cnt}${pgsubseg}">yCoor:</label>
                                <input class="form-control" type="text" id="subCoordinates-yCoor${pg}${cnt}${pgsubseg}" name="subCoordinates-yCoor${pg}${cnt}${pgsubseg}">
                                <label class="fontstyle"  for="subCoordinates-yCoor${pg}${cnt}${pgsubseg}">yCoor for the segment.</label></div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#subcoor'+ pg + cnt + pgsubseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pgsubimgid = 0;

function addPgSubImage(input) {
    pgsubimgid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="subimage${pg}${cnt}${pgsubseg}${pgsubimgid}">
                            <h2 >Image</h2>
                            <label class="fontstyle" >The image element is used for the need of taking an Image on the page that will not be an Artifact and can enter the Alternative Text for the image in here.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="subimg${pg}${cnt}${pgsubseg}" data-bs-toggle="collapse" href="#subimgcol${pg}${cnt}${pgsubseg}" data-bs-target="#subimgcol${pg}${cnt}${pgsubseg}" role="button" aria-expanded="true" aria-controls="subimgcol${pg}${cnt}${pgsubseg}">Image <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="subimgcol${pg}${cnt}${pgsubseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="subimage-altText${pg}${cnt}${pgsubseg}" >altText:</label>
                                <input class="form-control" type="text" id="subimage-altText${pg}${cnt}${pgsubseg}" name="subimage-altText${pg}${cnt}${pgsubseg}" required>
                                <label class="fontstyle"  for="subimage-altText${pg}${cnt}${pgsubseg}">The altText attribute is used for entering the altermative text for an image.</label></div><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePgSubImage(this)"><br><br>
                            </fieldset>
                        </div>`
    );
    $('#subimg'+ pg + cnt + pgsubseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var pgsublistid = 0;

function addPgSubListMarks(input) {
    pgsublistid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="sublistmarks${pg}${cnt}${pgsubseg}${pgsublistid}">
                            <h2 >ListMarks</h2>
                            <label  class="fontstyle">The listMarks is the element where an image is used for a book mark.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="sublstmarks${pg}${cnt}${pgsubseg}" data-bs-toggle="collapse" href="#sublstmarkscol${pg}${cnt}${pgsubseg}" data-bs-target="#sublstmarkscol${pg}${cnt}${pgsubseg}" role="button" aria-expanded="true" aria-controls="sublstmarkscol${pg}${cnt}${pgsubseg}">ListMarks <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="sublstmarkscol${pg}${cnt}${pgsubseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="sublistMarks-altText${pg}${cnt}${pgsubseg}" >altText:</label>
                                <input  type="text" id="sublistMarks-altText${pg}${cnt}${pgsubseg}" name="sublistMarks-altText${pg}${cnt}${pgsubseg}" required>
                                <label class="fontstyle"  for="sublistMarks-altText${pg}${cnt}${pgsubseg}">The altText attribute is for the alternative text for the listMark if an image is used.</label><br><br>
                                <label class="required-field" for="sublistMarks-type${pg}${cnt}${pgsubseg}" >type:</label>
                                <input  type="text" id="sublistMarks-type${pg}${cnt}${pgsubseg}" name="sublistMarks-type${pg}${cnt}${pgsubseg}" required>
                                <label class="fontstyle"  for="sublistMarks-type${pg}${cnt}${pgsubseg}">The type attribute is used to set the type of list, e.i. deciamal, roman,...etc.  For future use.</label></div><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removePgSubListMarks(this)"><br><br>
                            </fieldset>
                        </div>`
    );
    $('#sublstmarks'+ pg + cnt + pgsubseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

function removePageType(input) {
    input.parentNode.remove();
    pg -= 1;
}

function removeRow(input) {
    input.parentNode.remove();
}

function removePgSeg(input) {
    input.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
    cnt -= 1;
}

function removePgSubSeg(input) {
    input.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
    pgsubseg -= 1;
}

function removePgSubImage(input) {
    input.parentNode.parentNode.remove();
}

function removePgSubListMarks(input) {
    input.parentNode.parentNode.remove();
}

var colid = 1;
function addColumn() {
    document.querySelector("#form").insertAdjacentHTML(
        "beforeend",
        `<fieldset class="border p-2 form-group">
                                <legend id="coll${colid}" data-bs-toggle="collapse" href="#colid${colid}" data-bs-target="#colid${colid}" role="button" aria-expanded="true" aria-controls="colid${colid}">Column <i class="fas fa-minus float-right"  style="float: right;"></i></legend>
                                <div id="colid${colid}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="column-name" class="column">Name:</label>
                                <input class="form-control" type="text" class="column" id="column-name" name="column-name" required>
                                <label class="fontstyle" for="column-name" class="column">name for the column. Create names that make easy remember such as full,left or right.</label><br><br>
                                <label for="column-xCoor" class="column">xCoor:</label>
                                <input class="form-control" type="text" class="column" id="column-xCoor" name="column-xCoor">
                                <label class="fontstyle" for="column-xCoor" class="column">xCoor is the X position of the column using the measureUnit.</label><br><br>
                                <label for="column-width" class="column">Width:</label>
                                <input class="form-control" type="text" class="column" id="column-width" name="column-width">
                                <label class="fontstyle" for="width" class="column">width of the column in the distance of the measureUnit.</label></div><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                            </fieldset>`
    );
    $('#coll'+colid).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    colid += 1;
}

function removeColumnTag(input) {
    input.parentNode.parentNode.remove();
}

var cid = 1;
function addColumnTag() {
    document.querySelector("#divcolumn").insertAdjacentHTML(
        "beforeend",
        `<fieldset class="border p-2 form-group">
        <legend id="coll${cid}" data-bs-toggle="collapse" href="#colid${cid}" data-bs-target="#colid${cid}" role="button" aria-expanded="true" aria-controls="colid${cid}">Column <i class="fas fa-minus float-right"  style="float: right;"></i></legend>
                                <div id="colid${cid}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label for="column-name" class="required-field">Name:</label>
                                <input type="text" class="form-control" id="column-name" name="column-name" required>
                                <label class="fontstyle" for="column-name" class="column">name for the column.<br/>Create names that make easy remember such as full,left or right.</label><br><br>
                                <label for="column-xCoor" class="column">xCoor:</label>
                                <input type="text" class="form-control" id="column-xCoor" name="column-xCoor">
                                <label class="fontstyle" for="column-xCoor" class="column">xCoor is the X position of the column using the measureUnit.</label><br><br>
                                <label for="column-width" class="column">Width:</label>
                                <input type="text" class="form-control" id="column-width" name="column-width">
                                <label class="fontstyle" for="width" class="column">width of the column in the distance of the measureUnit.</label><br><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeColumnTag(this)"><br><br>
                                </div>
                            </fieldset>`
    );
    $('#coll'+cid).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
    cid += 1;
}

function displayColumn() {
    var x = document.getElementById("divcolumn");
    if (x.style.display === "none") {
        x.style.display = "block";
        $('#column-name').prop('required',true);
    } else {
        x.style.display = "none";
        $('#column-name').prop('required',false);
    }
}

function displayPageType() {
    var x = document.getElementById("divpagetype");
    if (x.style.display === "none") {
        x.style.display = "block";
        $('#pageType-name1').prop('required',true);
        $('#segment-name11').prop('required',true);
    } else {
        x.style.display = "none";
        $('#pageType-name1').prop('required',false);
        $('#segment-name11').prop('required',false);
    }
}

var pgseg = 1;
var seg = 1;
function addSegment(input) {
    seg += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="segment${pgseg}${seg}${seg}" class="segment${pgseg}${seg}${seg}">
                        <fieldset class="border p-2 form-group">
                        <legend id="seg${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#segmentcol${pgseg}${seg}${seg}" data-bs-target="#segmentcol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="segmentcol${pgseg}${seg}${seg}">Segment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                        <div id="segmentcol${pgseg}${seg}${seg}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="segment-name${pgseg}${seg}${seg}">Name:</label>
                            <input class="segmentcount form-control" type="text" id="segment-name${pgseg}${seg}${seg}" name="segment-name${pgseg}${seg}${seg}" required>
                            <label class="fontstyle"  for="segment-name${pgseg}${seg}${seg}">Segment name is the text used to find the Y coordinate(top) of the segment.</label></div><br>
                            <h2 >Segment Properties</h2>
                            <label class="fontstyle" >SegmentProperties are the settings used to capture the a segment correctly with the type of segment, coordinates calculation settings, and the header information.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <div id="segmentProperties${pgseg}${seg}${seg}" class="segmentProperties${pgseg}${seg}${seg}">
                                <legend id="segprop${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#segpropcol${pgseg}${seg}${seg}" data-bs-target="#segpropcol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="segpropcol${pgseg}${seg}${seg}">Segment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="segpropcol${pgseg}${seg}${seg}" class="collapse show">
                                <div class="form-group col-md-4 col-sm-8">
                                <label  for="segmentProperties-type${pgseg}${seg}${seg}">Type:</label>
                                <select class="form-select" id="segmentProperties-type${pgseg}${seg}${seg}" name="segmentProperties-type${pgseg}${seg}${seg}">
                                <option value></option>    
                                <option  value="combo">combo</option>
                                    <option  value="paragraph">paragraph</option>
                                    <option  value="image">image</option>
                                    <option  value="lines">lines</option>
                                    <option  value="table">table</option>
                                </select><br><br>
                                    <label  for="segmentProperties-duplicate${pgseg}${seg}${seg}">Duplicate:</label>
                                    <select class="form-select" id="segmentProperties-duplicate${pgseg}${seg}${seg}" name="segmentProperties-duplicate${pgseg}${seg}${seg}" >
										<option value selected>false</option>
										<option value="true">true</option>
									</select>
                                    <label class="fontstyle"  for="segmentProperties-duplicate${pgseg}${seg}${seg}">The duplicate attribute is used segment used the same beginning for a segment. Usually happens with a segmemnt type table.</label><br>
                                    <input id="seg_advanced_btn${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
									<div id="seg_advanced${pgseg}${seg}${seg}">
									<br />
                                    <label  for="segmentProperties-oneLineRowTable${pgseg}${seg}${seg}">oneLineRowTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-oneLineRowTable${pgseg}${seg}${seg}" name="segmentProperties-oneLineRowTable${pgseg}${seg}${seg}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-oneLineRowTable${pgseg}${seg}${seg}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                                    <label  for="segmentProperties-complexTable${pgseg}${seg}${seg}">complexTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-complexTable${pgseg}${seg}${seg}" name="segmentProperties-complexTable${pgseg}${seg}${seg}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-complexTable${pgseg}${seg}${seg}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                                    <label  for="segmentProperties-NeedMergeText${pgseg}${seg}${seg}">NeedMergeText:</label>
                                    <input class="form-control" type="text" id="segmentProperties-NeedMergeText${pgseg}${seg}${seg}" name="segmentProperties-NeedMergeText${pgseg}${seg}${seg}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-NeedMergeText${pgseg}${seg}${seg}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                                    <label  for="segmentProperties-phaseADesignTable${pgseg}${seg}${seg}">phaseADesignTable:</label>
                                    <input class="form-control" type="text" id="segmentProperties-phaseADesignTable${pgseg}${seg}${seg}" name="segmentProperties-phaseADesignTable${pgseg}${seg}${seg}" value="false">
                                    <label class="fontstyle"  for="segmentProperties-phaseADesignTable${pgseg}${seg}${seg}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label><br><br>
                                    <label  for="segmentProperties-processed${pgseg}${seg}${seg}">processed:</label>
                                    <input class="form-control" type="text" id="segmentProperties-processed${pgseg}${seg}${seg}" name="segmentProperties-processed${pgseg}${seg}${seg}" value="no" readonly>
                                    <label class="fontstyle"  for="segmentProperties-processed${pgseg}${seg}${seg}">The processed attribute is used internally in the app. It doesn't not need to exist in the individual templates.</label></div></div><br>
                                    <input id="addCoor${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Add Coordinates" onclick="addCoordinates(this)">
                                    <input id="addHeader${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Add Header" onclick="addHeader(this)">
                                    <input id="addImage${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addImage(this)">
                                    <input id="addListMarks${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Add ListMarks" onclick="addListMarks(this)">
                                    <input id="addSubSeg${pgseg}${seg}${seg}" class="btn btn-black btn-sm" type="button" value="Add SubSegment" onclick="addSubSegment(this)">
                                    <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeSeg(this)"><br><br>
                                </div>
                            </fieldset>
                            </div>
                        </fieldset>
                        </div>`
    );
    $('#seg'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#segprop'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#addCoor'+ pgseg + seg + seg).hide();
	$('#addHeader'+ pgseg + seg + seg).hide();
	$('#addImage'+ pgseg + seg + seg).hide();
	$('#addListMarks'+ pgseg + seg + seg).hide();
	$('#addSubSeg'+ pgseg + seg + seg).hide();

	$('#segmentProperties-type'+ pgseg + seg + seg).change(function(){
		console.log($(this).val());
		var type = $(this).val();
		if(type == 'paragraph')
		{
			$('#addImage'+ pgseg + seg + seg).hide();
			$('#addListMarks'+ pgseg + seg + seg).hide();
			$('#addSubSeg'+ pgseg + seg + seg).hide();
			$('#addCoor'+ pgseg + seg + seg).show();
			$('#addHeader'+ pgseg + seg + seg).show();
		}
		else if(type == 'combo')
		{
			$('#addImage'+ pgseg + seg + seg).hide();
			$('#addListMarks'+ pgseg + seg + seg).hide();
			$('#addCoor'+ pgseg + seg + seg).show();
			$('#addHeader'+ pgseg + seg + seg).show();
			$('#addSubSeg'+ pgseg + seg + seg).show();
		}
		else if(type == 'table')
		{
			$('#addImage'+ pgseg + seg + seg).hide();
			$('#addListMarks'+ pgseg + seg + seg).hide();
			$('#addSubSeg'+ pgseg + seg + seg).hide();
			$('#addCoor'+ pgseg + seg + seg).show();
			$('#addHeader'+ pgseg + seg + seg).show();
		}
		else if(type == 'lines')
		{
			$('#addImage'+ pgseg + seg + seg).hide();
			$('#addListMarks'+ pgseg + seg + seg).hide();
			$('#addSubSeg'+ pgseg + seg + seg).hide();
			$('#addCoor'+ pgseg + seg + seg).show();
			$('#addHeader'+ pgseg + seg + seg).show();
		}
		else if(type == 'image')
		{
			$('#addCoor'+ pgseg + seg + seg).hide();
			$('#addHeader'+ pgseg + seg + seg).hide();
			$('#addListMarks'+ pgseg + seg + seg).hide();
			$('#addSubSeg'+ pgseg + seg + seg).hide();
			$('#addImage'+ pgseg + seg + seg).show();
		}
        else
			{
                $('#addCoor'+ pgseg + seg + seg).hide();
                $('#addHeader'+ pgseg + seg + seg).hide();
                $('#addImage'+ pgseg + seg + seg).hide();
                $('#addListMarks'+ pgseg + seg + seg).hide();
                $('#addSubSeg'+ pgseg + seg + seg).hide();
			}
    });

    $('#seg_advanced'+ pgseg + seg + seg).hide();
	$("#seg_advanced_btn"+ pgseg + seg + seg).click(function() {
		$('#seg_advanced'+ pgseg + seg + seg).toggle();
	});
}

var subseg = 0;
function addSubSegment(input) {
    subseg += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div id="subsegment${pgseg}${seg}${seg}${subseg}" class="subsegment${pgseg}${seg}${seg}${subseg}">
                            <h2 >SubSegment</h2>
                            <label class="fontstyle" >subSegment(s) is the element that is used with a combo segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="subseg${pgseg}${seg}${seg}${subseg}" data-bs-toggle="collapse" href="#subsegcol${pgseg}${seg}${seg}${subseg}" data-bs-target="#subsegcol${pgseg}${seg}${seg}${subseg}" role="button" aria-expanded="true" aria-controls="subsegcol${pgseg}${seg}${seg}${subseg}">SubSegment <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="subsegcol${pgseg}${seg}${seg}${subseg}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="subSegment-name${pgseg}${seg}${seg}${subseg}">name:</label>
                            <input class="subsegmentcount form-control" type="text" id="subSegment-name${pgseg}${seg}${seg}${subseg}" name="subSegment-name${pgseg}${seg}${seg}${subseg}" required>
                            <label class="fontstyle"  for="subSegment-name${pgseg}${seg}${seg}${subseg}">Attribute name is the text used to find the Y coordinate(top) of the subSegment.</label></div><br>
                            <h2 >SubSegment Properties</h2>
                            <label class="fontstyle" >The properties to capture the text block of the subSegment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="subsegprop${pgseg}${seg}${seg}${subseg}" data-bs-toggle="collapse" href="#subsegpropcol${pgseg}${seg}${seg}${subseg}" data-bs-target="#subsegpropcol${pgseg}${seg}${seg}${subseg}" role="button" aria-expanded="true" aria-controls="subsegpropcol${pgseg}${seg}${seg}${subseg}">SubSegment Properties <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="subsegpropcol${pgseg}${seg}${seg}${subseg}" class="collapse show">
                            <div class="form-group col-md-4 col-sm-8">
                            <label class="required-field" for="subSegmentProperties-type${pgseg}${seg}${seg}${subseg}">type:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-type${pgseg}${seg}${seg}${subseg}" name="subSegmentProperties-type${pgseg}${seg}${seg}${subseg}" required><br>
                            <input id="subseg_advanced_btn${pgseg}${seg}${seg}${subseg}" class="btn btn-black btn-sm" type="button" value="Advanced Options"/>
							<div id="subseg_advanced${pgseg}${seg}${seg}${subseg}">
                            <br/>
                            <label  for="subSegmentProperties-oneLineRowTable${pgseg}${seg}${seg}${subseg}">oneLineRowTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-oneLineRowTable${pgseg}${seg}${seg}${subseg}" name="subSegmentProperties-oneLineRowTable${pgseg}${seg}${seg}${subseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-oneLineRowTable${pgseg}${seg}${seg}${subseg}">The oneLineRowTable attribute is used for tables when the data doesn't contain any lines between rows and each row is only one line. (Not used much)</label><br><br>
                            <label  for="subSegmentProperties-complexTable${pgseg}${seg}${seg}${subseg}">complexTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-complexTable${pgseg}${seg}${seg}${subseg}" name="subSegmentProperties-complexTable${pgseg}${seg}${seg}${subseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-complexTable${pgseg}${seg}${seg}${subseg}">The complexTable attribute is used for tables like the penalty table in CP14(Not used much)</label><br><br>
                            <label  for="subSegmentProperties-NeedMergeText${pgseg}${seg}${seg}${subseg}">NeedMergeText:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-NeedMergeText${pgseg}${seg}${seg}${subseg}" name="subSegmentProperties-NeedMergeText${pgseg}${seg}${seg}${subseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-NeedMergeText${pgseg}${seg}${seg}${subseg}">The needMergeText attribute is used for text that needs to be merged when the composition tool doesn't lay down words in one text item.(Not used much)</label><br><br>
                            <label  for="subSegmentProperties-phaseADesignTable${pgseg}${seg}${seg}${subseg}">phaseADesignTable:</label>
                            <input class="form-control" type="text" id="subSegmentProperties-phaseADesignTable${pgseg}${seg}${seg}${subseg}" name="subSegmentProperties-phaseADesignTable${pgseg}${seg}${seg}${subseg}" value="false">
                            <label class="fontstyle"  for="subSegmentProperties-phaseADesignTable${pgseg}${seg}${seg}${subseg}">The phaseADesignTable attribute is used when the tagging needs to process in the older table taggind. Need to test to file the correct layout. (Could change in the future whem more template are discovered)</label></div></div><br>
                            <input class="btn btn-black btn-sm" type="button" value="Add SubCoordinates" onclick="addSubCoordinates(this)">
                            <input class="btn btn-black btn-sm" type="button" value="Add Image" onclick="addSubImage(this)">
                            
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeSubSeg(this)"><br><br>
                        </div>
                        </fieldset>
                            </fieldset>
                            </div>`
    );
    $('#subseg'+ pgseg + seg + seg + subseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#subsegprop'+ pgseg + seg + seg + subseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });

    $('#subseg_advanced'+ pgseg + seg + seg + subseg).hide();
	$("#subseg_advanced_btn"+ pgseg + seg + seg + subseg).click(function() {
		$('#subseg_advanced'+ pgseg + seg + seg + subseg).toggle();
	});
}

var coorid = 0;

function addCoordinates(input) {
    coorid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="coordinates${pgseg}${seg}${seg}${coorid}">
                            <h2 >Coordinates</h2>
                            <label class="fontstyle" >The variables used in the coordinates section calculate the x, y, w, and h of the segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="coor${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#coorcol${pgseg}${seg}${seg}" data-bs-target="#coorcol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="coorcol${pgseg}${seg}${seg}">Coordinates <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="coorcol${pgseg}${seg}${seg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label  for="coordinates-calculate${pgseg}${seg}${seg}">calculate:</label>
                                <input class="form-control" type="text" id="coordinates-calculate${pgseg}${seg}${seg}" name="coordinates-calculate${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="coordinates-calculate${pgseg}${seg}${seg}">calculate attribute is set to true when the coordinates have to caclulated for the coordinates.</label><br><br>
                                <label  for="coordinates-closePhrase${pgseg}${seg}${seg}">closePhrase:</label>
                                <input class="form-control" type="text" id="coordinates-closePhrase${pgseg}${seg}${seg}" name="coordinates-closePhrase${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="coordinates-closePhrase${pgseg}${seg}${seg}">closePhrase attribute is set to the ending text or LINE. If there mutliple closePhrase they are split by "%".</label><br><br>
                                <label class="required-field" for="coordinates-column${pgseg}${seg}${seg}">column:</label>
                                <input class="form-control" type="text" id="coordinates-column${pgseg}${seg}${seg}" name="coordinates-column${pgseg}${seg}${seg}" required>
                                <label class="fontstyle"  for="coordinates-column${pgseg}${seg}${seg}">column is used to provide the xCoor and wLen for the segment.</label><br><br>
                                <label  for="coordinates-hLen${pgseg}${seg}${seg}">hLen:</label>
                                <input class="form-control" type="text" id="coordinates-hLen${pgseg}${seg}${seg}" name="coordinates-hLen${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="coordinates-hLen${pgseg}${seg}${seg}">hLen for the segment. For a combo the hLen is used for the height of the header.</label><br><br>
                                <label  for="coordinates-setCoordinates${pgseg}${seg}${seg}">setCoordinates:</label>
                                <input class="form-control" type="text" id="coordinates-setCoordinates${pgseg}${seg}${seg}" name="coordinates-setCoordinates${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="coordinates-setCoordinates${pgseg}${seg}${seg}">setCoordinates for the segment is an obsolete variable.</label><br><br>
                                <label  for="coordinates-yCoor${pgseg}${seg}${seg}">yCoor:</label>
                                <input class="form-control" type="text" id="coordinates-yCoor${pgseg}${seg}${seg}" name="coordinates-yCoor${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="coordinates-yCoor${pgseg}${seg}${seg}">yCoor for the segment.</label></div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#coor'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var headid = 0;

function addHeader(input) {
    headid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="header${pgseg}${seg}${seg}${headid}">
                            <h2 >Header</h2>
                            <label class="fontstyle" >The variables used in the header determine the header type of the segment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="head${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#headercol${pgseg}${seg}${seg}" data-bs-target="#headercol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="headercol${pgseg}${seg}${seg}">Header <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="headercol${pgseg}${seg}${seg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field"  for="header-active${pgseg}${seg}${seg}">active:</label>
                                <input class="form-control" type="text" id="header-active${pgseg}${seg}${seg}" name="header-active${pgseg}${seg}${seg}" required>
                                <label class="fontstyle"  for="header-active${pgseg}${seg}${seg}">active is set to true if a header of exists in the segment.</label><br><br>
                                <label  for="header-bookmarkText${pgseg}${seg}${seg}">bookmarkText:</label>
                                <input class="form-control" type="text" id="header-bookmarkText${pgseg}${seg}${seg}" name="header-bookmarkText${pgseg}${seg}${seg}">
                                <label class="fontstyle"  for="header-bookmarkText${pgseg}${seg}${seg}">bookmarkText attribute is needed when the header contains a M-dash or a "'". They will not show up correction in the bookMark in Adobe.</label><br><br>
                                <label for="header-level${pgseg}${seg}${seg}">level:</label>
                                <select class="form-select" id="header-level${pgseg}${seg}${seg}" name="header-level${pgseg}${seg}${seg}">
                                <option value></option>    
                                <option  value="H1">H1</option>
                                    <option  value="H2">H2</option>
                                    <option  value="H3">H3</option>
                                    <option  value="H4">H4</option>
                                </select><br>
                                <label  for="header-type${pgseg}${seg}${seg}">type:</label>
                                <select class="form-select" id="header-type${pgseg}${seg}${seg}" name="header-type${pgseg}${seg}${seg}">
                                <option value></option>
                                <option  value="top">top</option>
                                    <option  value="topleft">topleft</option>
                                    <option  value="left">left</option>
                                    <option  value="first">first</option>
                                    <option  value="notice">notice</option>
                                </select>
                                </div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#head'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var imgid = 0;

function addImage(input) {
    imgid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="image${pgseg}${seg}${seg}${imgid}">
                            <h2 >Image</h2>
                            <label class="fontstyle" >The image element is used for the need of taking an Image on the page that will not be an Artifact and can enter the Alternative Text for the image in here.</label><br><br>
                            <fieldset class="border p-2 form-group">
                            <legend id="img${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#imagecol${pgseg}${seg}${seg}" data-bs-target="#imagecol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="imagecol${pgseg}${seg}${seg}">Image <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                            <div id="imagecol${pgseg}${seg}${seg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="image-altText${pgseg}${seg}${seg}">altText:</label>
                                <input class="form-control" type="text" id="image-altText${pgseg}${seg}${seg}" name="image-altText${pgseg}${seg}${seg}" required>
                                <label class="fontstyle"  for="image-altText${pgseg}${seg}${seg}">The altText attribute is used for entering the altermative text for an image.</label></div><br>
                            </fieldset>    
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#img'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var listid = 0;

function addListMarks(input) {
    listid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="listMarks${pgseg}${seg}${seg}${listid}">
                            <h2 >ListMarks</h2>
                                <label class="fontstyle" >The listMarks is the element where an image is used for a book mark.</label><br><br>
                                <fieldset class="border p-2 form-group">
                                    <legend id="lstmarks${pgseg}${seg}${seg}" data-bs-toggle="collapse" href="#lstmarkscol${pgseg}${seg}${seg}" data-bs-target="#lstmarkscol${pgseg}${seg}${seg}" role="button" aria-expanded="true" aria-controls="lstmarkscol${pgseg}${seg}${seg}">ListMarks <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                    <div id="lstmarkscol${pgseg}${seg}${seg}" class="form-group col-md-4 col-sm-8 collapse show">
                                    <label class="required-field" for="listMarks-altText${pgseg}${seg}${seg}">altText:</label>
                                    <input class="form-control" type="text" id="listMarks-altText${pgseg}${seg}${seg}" name="listMarks-altText${pgseg}${seg}${seg}" required>
                                    <label class="fontstyle"  for="listMarks-altText${pgseg}${seg}${seg}">The altText attribute is for the alternative text for the listMark if an image is used.</label><br><br>
                                    <label class="required-field" for="listMarks-type${pgseg}${seg}${seg}">type:</label>
                                    <input class="form-control" type="text" id="listMarks-type${pgseg}${seg}${seg}" name="listMarks-type${pgseg}${seg}${seg}" required>
                                    <label class="fontstyle"  for="listMarks-type${pgseg}${seg}${seg}">The type attribute is used to set the type of list, e.i. deciamal, roman,...etc.  For future use.</label></div><br>
                                </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#lstmarks'+ pgseg + seg + seg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var subcoorid = 0;

function addSubCoordinates(input) {
    subcoorid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="subcoordinates${pgseg}${seg}${seg}${subseg}${subcoorid}">
                            <h2 >SubCoordinates</h2>
                            <label class="fontstyle" >The variables used in the coordinates section calculate the x, y, w, and h of the subSegment.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="subcoor${pgseg}${seg}${seg}${subseg}" data-bs-toggle="collapse" href="#subcoorcol${pgseg}${seg}${seg}${subseg}" data-bs-target="#subcoorcol${pgseg}${seg}${seg}${subseg}" role="button" aria-expanded="true" aria-controls="subcoorcol${pgseg}${seg}${seg}${subseg}">SubCoordinates <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="subcoorcol${pgseg}${seg}${seg}${subseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="subCoordinates-calculate${pgseg}${seg}${seg}${subseg}">calculate:</label>
                                <input class="form-control" type="text" id="subCoordinates-calculate${pgseg}${seg}${seg}${subseg}" name="subCoordinates-calculate${pgseg}${seg}${seg}${subseg}" required>
                                <label class="fontstyle"  for="subCoordinates-calculate${pgseg}${seg}${seg}${subseg}">The variables used in the subCoordinates section calculate the x, y, w, and h of the segment.</label><br><br>
                                <label  for="subCoordinates-closePhrase${pgseg}${seg}${seg}${subseg}">closePhrase:</label>
                                <input class="form-control" type="text" id="subCoordinates-closePhrase${pgseg}${seg}${seg}${subseg}" name="subCoordinates-closePhrase${pgseg}${seg}${seg}${subseg}">
                                <label class="fontstyle"  for="subCoordinates-closePhrase${pgseg}${seg}${seg}${subseg}">closePhrase attribute is set to the ending text or LINE. If there mutliple closePhrase they are split by "%".</label><br><br>
                                <label class="required-field" for="subCoordinates-column${pgseg}${seg}${seg}${subseg}">column:</label>
                                <input class="form-control" type="text" id="subCoordinates-column${pgseg}${seg}${seg}${subseg}" name="subCoordinates-column${pgseg}${seg}${seg}${subseg}" required>
                                <label class="fontstyle"  for="subCoordinates-column${pgseg}${seg}${seg}${subseg}">column is used to provide the xCoor and wLen for the subSegment.</label><br><br>
                                <label  for="subCoordinates-hLen${pgseg}${seg}${seg}${subseg}">hLen:</label>
                                <input class="form-control" type="text" id="subCoordinates-hLen${pgseg}${seg}${seg}${subseg}" name="subCoordinates-hLen${pgseg}${seg}${seg}${subseg}">
                                <label class="fontstyle"  for="subCoordinates-hLen${pgseg}${seg}${seg}${subseg}">hLen for the subSegment. For a combo the hLen is used for the height of the header.</label><br><br>
                                <label  for="subCoordinates-yCoor${pgseg}${seg}${seg}${subseg}">yCoor:</label>
                                <input class="form-control" type="text" id="subCoordinates-yCoor${pgseg}${seg}${seg}${subseg}" name="subCoordinates-yCoor${pgseg}${seg}${seg}${subseg}">
                                <label class="fontstyle"  for="subCoordinates-yCoor${pgseg}${seg}${seg}${subseg}">yCoor for the segment.</label></div><br>
                            </fieldset>
                            <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeRow(this)"><br><br>
                        </div>`
    );
    $('#subcoor'+ pgseg + seg + seg + subseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var subimgid = 0;

function addSubImage(input) {
    subimgid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="subimage${pgseg}${seg}${seg}${subseg}${subimgid}">
                            <h2 >Image</h2>
                            <label class="fontstyle" >The image element is used for the need of taking an Image on the page that will not be an Artifact and can enter the Alternative Text for the image in here.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="subimg${pgseg}${seg}${seg}${subseg}" data-bs-toggle="collapse" href="#subimgcol${pgseg}${seg}${seg}${subseg}" data-bs-target="#subimgcol${pgseg}${seg}${seg}${subseg}" role="button" aria-expanded="true" aria-controls="subimgcol${pgseg}${seg}${seg}${subseg}">Image <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="subimgcol${pgseg}${seg}${seg}${subseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="subimage-altText${pgseg}${seg}${seg}${subseg}">altText:</label>
                                <input class="form-control" type="text" id="subimage-altText${pgseg}${seg}${seg}${subseg}" name="subimage-altText${pgseg}${seg}${seg}${subseg}" required>
                                <label class="fontstyle"  for="subimage-altText${pgseg}${seg}${seg}${subseg}">The altText attribute is used for entering the altermative text for an image.</label></div><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeSubImage(this)"><br><br>
                            </fieldset>
                        </div>`
    );
    $('#subimg'+ pgseg + seg + seg + subseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

var sublistid = 0;

function addSubListMarks(input) {
    sublistid += 1;
    var tag = $(input).parent().attr("id");
    document.querySelector(`#${tag}`).insertAdjacentHTML(
        "beforeend",
        `<div class="sublistmarks${pgseg}${seg}${seg}${subseg}${sublistid}">
                            <h2 >ListMarks</h2>
                            <label class="fontstyle" >The listMarks is the element where an image is used for a book mark.</label><br><br>
                            <fieldset class="border p-2 form-group">
                                <legend id="sublstmarks${pgseg}${seg}${seg}${subseg}" data-bs-toggle="collapse" href="#sublstmarkscol${pgseg}${seg}${seg}${subseg}" data-bs-target="#sublstmarkscol${pgseg}${seg}${seg}${subseg}" role="button" aria-expanded="true" aria-controls="sublstmarkscol${pgseg}${seg}${seg}${subseg}">ListMarks <i class="fas fa-minus float-right" style="float: right;"></i></legend>
                                <div id="sublstmarkscol${pgseg}${seg}${seg}${subseg}" class="form-group col-md-4 col-sm-8 collapse show">
                                <label class="required-field" for="sublistMarks-altText${pgseg}${seg}${seg}${subseg}">altText:</label>
                                <input class="form-control" type="text" id="sublistMarks-altText${pgseg}${seg}${seg}${subseg}" name="sublistMarks-altText${pgseg}${seg}${seg}${subseg}" required>
                                <label class="fontstyle"  for="sublistMarks-altText${pgseg}${seg}${seg}${subseg}">The altText attribute is for the alternative text for the listMark if an image is used.</label><br><br>
                                <label class="required-field" for="sublistMarks-type${pgseg}${seg}${seg}${subseg}">type:</label>
                                <input class="form-control" type="text" id="sublistMarks-type${pgseg}${seg}${seg}${subseg}" name="sublistMarks-type${pgseg}${seg}${seg}${subseg}" required>
                                <label class="fontstyle"  for="sublistMarks-type${pgseg}${seg}${seg}${subseg}">The type attribute is used to set the type of list, e.i. deciamal, roman,...etc.  For future use.</label></div><br>
                                <input class="btn btn-black btn-sm" type="button" value="Remove" onclick="removeSubListMarks(this)"><br><br>
                            </fieldset>
                        </div>`
    );
    $('#sublstmarks'+ pgseg + seg + seg + subseg).click(function() {
        $(this).children().toggleClass('fas fa-plus fas fa-minus');
    });
}

function removePageType(input) {
    input.parentNode.remove();
    pg -= 1;
}

function removeRow(input) {
    input.parentNode.remove();
}

function removeSeg(input) {
    input.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
    seg -= 1;
}

function removeSubSeg(input) {
    input.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
    subseg -= 1;
}

function removeSubImage(input) {
    input.parentNode.parentNode.remove();
}

function removeSubListMarks(input) {
    input.parentNode.parentNode.remove();
}

window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});