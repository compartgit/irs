const {
	RSA_X931_PADDING
} = require('constants');
var express = require('express');
var router = express.Router();
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const multer  = require('multer');
const fs = require("fs");
const tempdir = './templatefiles';
const coldir = './includefiles/columns';
const pgdir = './includefiles/pagetype';
const segdir = './includefiles/segments';
const Controller = require('../controller/controller');
const columnController = require('../controller/columncontroller');
const segmentController = require('../controller/segmentcontroller');
const pagetypeController = require('../controller/pagetypecontroller');
const templateController = require('../controller/templatecontroller');

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'templatefiles/')
	},
	filename: function (req, file, cb) {
		var cpFileNames = fs.readdirSync(tempdir);
		var flag = 0;
		for(var f in cpFileNames)
		{
			if(cpFileNames[f] == file.originalname)
			{
				flag = 1;
			}
		}
		if(flag == 1)
			cb(null, 'copy of '+ file.originalname);
		else
			cb(null, file.originalname );
	}
});

var colstorage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'includefiles/columns/')
	},
	filename: function (req, file, cb) {
		var colFileNames = fs.readdirSync(coldir);
		var flag = 0;
		for(var f in colFileNames)
		{
			if(colFileNames[f] == file.originalname)
			{
				flag = 1;
			}
		}
		if(flag == 1)
			cb(null, 'copy of '+ file.originalname);
		else
			cb(null, file.originalname );
	}
});

var pgstorage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'includefiles/pagetype/')
	},
	filename: function (req, file, cb) {
		var pgFileNames = fs.readdirSync(pgdir);
		var flag = 0;
		for(var f in pgFileNames)
		{
			if(pgFileNames[f] == file.originalname)
			{
				flag = 1;
			}
		}
		if(flag == 1)
			cb(null, 'copy of '+ file.originalname);
		else
			cb(null, file.originalname );
	}
});

var segstorage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'includefiles/segments/')
	},
	filename: function (req, file, cb) {
		var segFileNames = fs.readdirSync(segdir);
		var flag = 0;
		for(var f in segFileNames)
		{
			if(segFileNames[f] == file.originalname)
			{
				flag = 1;
			}
		}
		if(flag == 1)
			cb(null, 'copy of '+ file.originalname);
		else
			cb(null, file.originalname );
	}
});

const upload = multer({ storage: storage });
const colupload = multer({ storage: colstorage });
const pgupload = multer({ storage: pgstorage });
const segupload = multer({ storage: segstorage });

// Set up Global configuration access
dotenv.config();

function middleware (req, res, next) {
	let tokenHeaderKey = process.env.TOKEN_HEADER_KEY;
	let jwtSecretKey = process.env.JWT_SECRET_KEY;

	try {
		const token = req.header(tokenHeaderKey);
		const verified = jwt.verify(token, jwtSecretKey);
		if(verified){
			next();
		}else{
			// Access Denied
			return res.status(401).send(error);
		}
	} catch (error) {
		// Access Denied
		return res.status(401).send(error);
	}
}

/* GET home page. */
router.get('/', function(req, res, next) {
	Controller.getLogin(req, res);
});

/* GET register page. */
router.get('/register', function(req, res, next) {
	Controller.getRegister(req, res);
});

/* GET login page. */
router.get('/login', function(req, res, next) {
	Controller.getLogin(req, res);
});

/* GET forgot password page. */
router.get('/forgotpassword', function(req, res, next) {
	Controller.getForgotPassword(req, res);
});

/* GET User Info with email */
router.get('/finduser/:id', function(req, res, next) {
	Controller.getUserInfo(req, res);
});

/* GET dashboard page. */
router.get('/dashboard', function(req, res, next) {
	templateController.Dashboard(req, res);
});

/* GET dashboard page. */
router.get('/getdashboard', middleware, function(req, res, next) {
	templateController.getDashboard(req, res);
});

/* GET profile page. */
router.get('/profile', function(req, res, next) {
	Controller.Profile(req, res);
});

/* GET profile page. */
router.get('/getprofile', middleware, function(req, res, next) {
	Controller.getProfile(req, res);
});

/* GET CP File page. */
router.get('/api/cpfile', function(req, res, next) {
	templateController.CPFile(req, res);
});

/* GET CP File page. */
router.get('/api/getcpfile', middleware, function(req, res, next) {
	templateController.getCPFile(req, res);
});

/* GET Column Include page. */
router.get('/api/columninclude', function(req, res, next) {
	columnController.ColumnInclude(req, res);
});

/* GET Column Include page. */
router.get('/api/getcolumninclude', middleware, function(req, res, next) {
	columnController.getColumnInclude(req, res);
});

/* GET PageType Include page. */
router.get('/api/pagetypeinclude', function(req, res, next) {
	pagetypeController.PageTypeInclude(req, res);
});

/* GET PageType Include page. */
router.get('/api/getpagetypeinclude', middleware, function(req, res, next) {
	pagetypeController.getPageTypeInclude(req, res);
});

/* GET Segment Include page. */
router.get('/api/segmentinclude', function(req, res, next) {
	segmentController.SegmentInclude(req, res);
});

/* GET Segment Include page. */
router.get('/api/getsegmentinclude', middleware, function(req, res, next) {
	segmentController.getSegmentInclude(req, res);
});

/* GET column page. */
router.get('/api/column', function(req, res, next) {
	columnController.ColumnForm(req, res);
});

/* GET column page. */
router.get('/api/getcolumn', middleware, function(req, res, next) {
	columnController.getColumnForm(req, res);
});

/* GET pagetype page. */
router.get('/api/pagetype', function(req, res, next) {
	pagetypeController.PageTypeForm(req, res);
});

/* GET pagetype page. */
router.get('/api/getpagetype', middleware, function(req, res, next) {
	pagetypeController.getPageTypeForm(req, res);
});

/* GET segment page. */
router.get('/api/segment', function(req, res, next) {
	segmentController.SegmentForm(req, res);
});

/* GET segment page. */
router.get('/api/getsegment', middleware, function(req, res, next) {
	segmentController.getSegmentForm(req, res);
});

/* GET javascript code. */
router.get('/api/script', function(req, res, next) {
	Controller.getScript(req, res);
});

/* GET css code. */
router.get('/api/css', function(req, res, next) {
	Controller.getStylesheet(req, res);
});

// GET file names
router.get('/api/getfilenames', middleware, function(req, res, next) {
	Controller.getFileNames(req, res);
});

// GET the Download Template File by ID
router.get('/api/downloadfile/:id', middleware, function(req, res, next) {
	templateController.getDownloadFile(req, res);
});

// GET the Download Column File by ID
router.get('/api/downloadcolumnfile/:id', middleware, function(req, res, next) {
	columnController.getDownloadColFile(req, res);
});

// GET the Download PageType File by ID
router.get('/api/downloadpagetypefile/:id', middleware, function(req, res, next) {
	pagetypeController.getDownloadPgFile(req, res);
});

// GET the Download Segment File by ID
router.get('/api/downloadsegmentfile/:id', middleware, function(req, res, next) {
	segmentController.getDownloadSegFile(req, res);
});

// GET Copy Template File by ID
router.get('/api/copytemplatefile/:id', function(req, res, next) {
	templateController.getCopyTemplateFile(req, res);
});

// GET the Edit Template page
router.get('/api/edittemplate/:id', middleware, function(req, res, next) {
	templateController.getEditTemplateForm(req, res);
});

// GET the Edit Column page
router.get('/api/editcolumn/:id', middleware, function(req, res, next) {
	columnController.getEditColumnForm(req, res);
});

// GET the Edit PageType page
router.get('/api/editpagetype/:id', middleware, function(req, res, next) {
	pagetypeController.getEditPageTypeForm(req, res);
});

// GET the Edit Segment page
router.get('/api/editsegment/:id', middleware, function(req, res, next) {
	segmentController.getEditSegmentForm(req, res);
});

// DELETE the CP file
router.delete('/api/deletecpfile/:id', middleware, function(req, res, next) {
	templateController.deleteCPFile(req, res);
});

// DELETE the column include file
router.delete('/api/deletecolumn/:id', middleware, function(req, res, next) {
	columnController.deleteColumnInclude(req, res);
});

// DELETE the pagetype include file
router.delete('/api/deletepagetype/:id', middleware, function(req, res, next) {
	pagetypeController.deletePageTypeInclude(req, res);
});

// DELETE the segment include file
router.delete('/api/deletesegment/:id', middleware, function(req, res, next) {
	segmentController.deleteSegmentInclude(req, res);
});

// POST Registration data
router.post('/postregister', function(req, res, next) {
	Controller.postRegister(req, res);
});

// POST Forgot Password data
router.post('/postforgotpass', function(req, res, next) {
	Controller.postForgotPassword(req, res);
});

// POST Login data
router.post('/postlogin', function(req, res, next) {
	Controller.postLogin(req, res);
});

// POST update profile data
router.post('/postprofile', function(req, res, next) {
	Controller.postProfile(req, res);
});

// POST template data
router.post('/api/posttemplate', function(req, res, next) {
	templateController.postTemplate(req, res);
});

// POST import template file data
router.post('/api/postimportfile', upload.single('importfile'), function(req, res, next) {
	templateController.postImportTemplate(req, res);
});

// POST import column file data
router.post('/api/postimportcolfile', colupload.single('importcolfile'), function(req, res, next) {
	columnController.postImportColumn(req, res);
});

// POST import pagetype file data
router.post('/api/postimportpgfile', pgupload.single('importpgfile'), function(req, res, next) {
	pagetypeController.postImportPageType(req, res);
});

// POST import segment file data
router.post('/api/postimportsegfile', segupload.single('importsegfile'), function(req, res, next) {
	segmentController.postImportSegment(req, res);
});

// POST column data
router.post('/api/postcolumn', function(req, res, next) {
	columnController.postColumn(req, res);
});

// POST pagetype data
router.post('/api/postpagetype', function(req, res, next) {
	pagetypeController.postPageType(req, res);
});

// POST segment data
router.post('/api/postsegment', function(req, res, next) {
	segmentController.postSegment(req, res);
});

// POST Edit template data
router.post('/api/postedittemplatedata', function(req, res, next) {
	templateController.postEditTemplate(req, res);
});

// POST Edit Column data
router.post('/api/posteditcolumndata', function(req, res, next) {
	columnController.postEditColumn(req, res);
});

// POST Edit PageType data
router.post('/api/posteditpagetypedata', function(req, res, next) {
	pagetypeController.postEditPageType(req, res);
});

// POST Edit Segment data
router.post('/api/posteditsegmentdata', function(req, res, next) {
	segmentController.postEditSegment(req, res);
});

module.exports = router;